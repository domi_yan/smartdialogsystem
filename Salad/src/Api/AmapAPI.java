package Api;
import java.util.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.JSONArray;

import util.HttpRequest;

public class AmapAPI {

	public static JSONObject getPosition(String city, String description) throws Exception{
		//example "http://restapi.amap.com/v3/geocode/geo?address=北京市朝阳区阜通东大街6号&output=json&key=7b9f4cb87fa92655443aeb85314285d5"
		// String s=HttpRequest.sendGet("http://localhost:6144/Home/RequestString", "key=123&v=456");
		String url = "http://restapi.amap.com/v3/geocode/geo";
		String parameter = "address=" + description + 
				"&city=" + city + 
				"&output=json&key=7b9f4cb87fa92655443aeb85314285d5";
		String retStr = HttpRequest.sendGet(url, parameter);
		JSONObject obj = (JSONObject) JSONValue.parse(retStr);
		return obj;
	}
	
	public static Integer getNumLocation(JSONObject obj){
		int locationNum = ((JSONArray) obj.get("geocodes")).size();
		return locationNum;
	}
	
	/* 
	 * 高德API Spec
	 * level           示例说明
      	国家             中国
		省               河北省、北京市
		市               宁波市	
		区县             北京市朝阳区
		开发区           亦庄经济开发区
		乡镇             回龙观镇
		村庄             三元村
		热点商圈         上海市黄浦区老西门
		兴趣点            北京市朝阳区奥林匹克公园(南门)
		门牌号            朝阳区阜通东大街6号
		单元号            望京西园四区5号楼2单元
		道路              北京市朝阳区阜通东大街
		道路交叉路口       北四环西路辅路/善缘街
		公交站台、地铁站    海淀黄庄站A1西北口
		未知              未确认级别的POI
	 */
	public static String getLevel(JSONObject obj){
		if(getNumLocation(obj) > 0){
			String levelStr = (String) ((JSONObject)((JSONArray) obj.get("geocodes")).get(0)).get("level");
			return levelStr;
		}
		return "未知";
	}
	
	public static boolean isPreciseLocation(JSONObject obj){
		String s = getLevel(obj);
		for(int i = 0; i < preciseLevels.length; i++){
			if(s.equals(preciseLevels[i]))
				return true;
		}
		return false;	
	}
	
	static private String[] preciseLevels = {"单元号", "门牌号"};
}
