package Api;

import org.junit.Test;
import org.json.simple.JSONObject;

public class AmapAPITest {

	@Test
	public void testGetPosition() throws Exception{
		String[] addressList = {
				"湖北省",
				"武汉市开发区",
				"北京市建国门外大街",
				"北京市建国门外大街1号国贸大厦1座34层101",
				"顺义区天竺空港工业区天柱路10号海关楼226室",
				"北京北京市北三环中路27号商房大厦6层632室",
				"中国北京市朝阳区东三环中路39号建外SOHO",
				"北京市朝阳区呼家楼京广中心3013室",
				"西大望路1号温特莱中心B座909",
				"海淀区科学院南路2号",
				"北京市海淀区知春路23号量子银座409",
				"北京海淀区知春路甲48号盈都大厦C座1－16B",
				"北京市海淀区学院路35号世宁大厦906座",
				"五道口",
				"五棵松",
				"您好",
		};
		for(int i = 0; i < addressList.length; i++){
			JSONObject obj = AmapAPI.getPosition("北京", addressList[i]);
			System.out.println("Test: " + addressList[i]);
			System.out.println("num location return: " + AmapAPI.getNumLocation(obj).toString());
			System.out.println("level of first returned: " + AmapAPI.getLevel(obj));
			System.out.println("is a precise location?:" + AmapAPI.isPreciseLocation(obj));

			
		}
	}

}
