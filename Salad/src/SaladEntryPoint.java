import py4j.GatewayServer;
import java.util.*;

public class SaladEntryPoint {
	private GoodSalad salad;
	
	public GoodSalad getSalad() { 
		return salad;
	}
	
	private ArrayList<GoodSalad> salads;
	
	public SaladEntryPoint() {
		salads = new ArrayList<GoodSalad>();	
	}
	
	public GoodSalad getNewSalad(String rulName) { 
		GoodSalad s = new GoodSalad(rulName);
		salads.add(s);
		return s;
	}
	
	public Integer getCurrentId() {
		return salads.size() - 1;
	}
	
	public GoodSalad getSaladById(int idx){
		return salads.get(idx);
	}
	
	public static void main(String[] args) {
		GatewayServer gatewayServer = new GatewayServer(new SaladEntryPoint());
        gatewayServer.start();
        System.out.println("Gateway Server Started");
	}
}
