/*-***
 * JFlex file of Rule
 * Author: Yue Yan
 */
 
import java_cup.runtime.*;

%%
/*-*
 * LEXICAL FUNCTIONS:
 */

%cup
%line
%column
%unicode
%class RuleLexer

%{
StringBuffer string = new StringBuffer();

/**
 * Return a new Symbol with the given token id, and with the current line and
 * column numbers.
 */
Symbol newSym(int tokenId) {
    return new Symbol(tokenId, yyline, yycolumn);
}

/**
 * Return a new Symbol with the given token id, the current line and column
 * numbers, and the given token value.  The value is used for tokens such as
 * identifiers and numbers.
 */
Symbol newSym(int tokenId, Object value) {
    return new Symbol(tokenId, yyline, yycolumn, value);
}

%}

/*-*
 * PATTERN DEFINITIONS:
 */
    left_identifier  = \<
    right_identifier = \>

    letter          = [A-Za-z]
    digit           = [0-9]
    alphanumeric    = {letter}|{digit}
    other_id_char   = [_]
    dollar          = [$]
    variable        = {letter}({alphanumeric}|{other_id_char})*
    identifier      = {variable}(\.{variable})*

    chinese_char    = [\u4e00-\u9fa5]
    chinese_word    = {chinese_char}+

	any_str_in_brkt = [^\<]+
    

    parameter       = {dollar}{identifier}
    integer         = {digit}+
    real            = {integer}\.{integer}
    rule_name       = {left_identifier}{identifier}({any_str_in_brkt}){0,1}{right_identifier} /* <food> */
    

    whitespace      = [ \n\t]

    /* comments */
    Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

    LineTerminator = \r|\n|\r\n
    InputCharacter = [^\r\n]
    TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
    // Comment can be the last line of the file, without line terminator.
    EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}?
    DocumentationComment = "/**" {CommentContent} "*"+ "/"
    CommentContent       = ( [^*] | \*+ [^/*] )*

%state STRING

%%
/**
 * LEXICAL RULES:
 */
<YYINITIAL> {
    /* keywords */
    "namespace"       { return newSym(sym.NAMESPACE, "namespace"); }
    "system"          { return newSym(sym.SYSTEM, "system"); }
    "!start"          { return newSym(sym.E_START, "!start"); }
    "!score"          { return newSym(sym.E_SCORE, "!score"); }
    "!out"            { return newSym(sym.E_OUT, "!out"); }

    \"                { string.setLength(0); yybegin(STRING); }
    
        /* operators */
    "RAW_TEXT"      { return newSym(sym.OP_RAW_TEXT, "RAW_TEXT");}
    "MERGE"         { return newSym(sym.OP_MERGE, "MERGE"); }
    "AND"           { return newSym(sym.OP_AND, "AND"); }
    "ADD"           { return newSym(sym.OP_ADD, "ADD"); }
    "SUB"           { return newSym(sym.OP_SUB, "SUB"); }
    "CHANGE"        { return newSym(sym.OP_CHANGE, "CHANGE"); }
    "OR"            { return newSym(sym.OP_OR, "OR"); }
    
    /* identifier */
    {chinese_word}    { return newSym(sym.CHINESE_WORD, yytext()); }
    {identifier}      { return newSym(sym.IDENTIFIER, yytext() ); }
    {rule_name}       { return newSym(sym.RULE_NAME, yytext() ); }
    {parameter}       { return newSym(sym.PARAMETER, yytext() ); }
    {integer}        { return newSym(sym.INTEGER, yytext() ); }
    

    "{"             { return newSym(sym.LEFT_BIG_PAREN, "{"); }
    "}"             { return newSym(sym.RIGHT_BIG_PAREN, "}"); }
    "("             { return newSym(sym.LEFT_PAREN, "("); }
    ")"             { return newSym(sym.RIGHT_PAREN, ")"); }
    "["             { return newSym(sym.LEFT_REC_PAREN, "["); }
    "]"             { return newSym(sym.RIGHT_REC_PAREN, "]"); }
    "|"             { return newSym(sym.SELECT, "|"); }
    "||"            { return newSym(sym.DEFINE_SEMANTIC, "||"); }
    ";"             { return newSym(sym.END_STMT, ";"); }
    "::="           { return newSym(sym.EXPAND, "::="); }
    ":"             { return newSym(sym.COLON, ":"); }
    ","             { return newSym(sym.COMMA, ","); }
    "+"             { return newSym(sym.CONNECT, "+"); }


    /* comments */
    {Comment}       { /* Ignore Comment */ }






    {whitespace}    { /* Ignore whitespace. */ }
    .               { System.out.println("Illegal char, '" + yytext() +
                        "' line: " + yyline + ", column: " + yychar); }
}

<STRING> {
      \"                             { yybegin(YYINITIAL); 
                                       return newSym(sym.STRING_LITERAL, 
                                       string.toString()); }
      [^\n\r\"\\]+                   { string.append( yytext() ); }
      \\t                            { string.append('\t'); }
      \\n                            { string.append('\n'); }

      \\r                            { string.append('\r'); }
      \\\"                           { string.append('\"'); }
      \\                             { string.append('\\'); }
}
