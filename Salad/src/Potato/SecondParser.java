package Potato;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import Potato.extractor.ExtractorManager;
import Potato.protos.JSONHelper;
import Potato.protos.PotatoResult;
import Potato.protos.Unit;
import Potato.protos.Rule;

public class SecondParser {
	private static final Logger log = Logger.getLogger(SecondParser.class.getName());
	private ArrayList<Rule> rules;
	private HashMap<String, Integer> ruleId;
	private PotatoResult dp[][][];
	private boolean visited[][][];
	private ExtractorManager extractorManager;
	
	// Current input string.
	private String input;
	
	public boolean isRulesValid() {
		ruleId = new HashMap<String, Integer>();
		for (Rule rule : rules) {
			if (ruleId.containsKey(rule.name)) continue;
			ruleId.put(rule.name, ruleId.size());
		}
		for (Rule rule : rules) {
			for (ArrayList<Unit> units : rule.expressions) {
				for (Unit u: units) {
					if (u.isTerminal() == false && u.isBuiltin() == false 
							&& ruleId.containsKey(u.name) == false) {
						log.log(Level.WARNING, String.format(
								"%s is not defined in the given rule file", u.name));
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public SecondParser(ArrayList<Rule> _rules) {
		this.rules = _rules;
	}
	
	public PotatoResult getPotatoResult(String _input) {
		if (this.isRulesValid() == false) return null;
		
		this.input = _input;
		this.extractorManager = new ExtractorManager();
		this.extractorManager.init(_input);
		
		PotatoResult ret = null;
		dpMain();
		for (int k = 0; k < rules.size(); k++) {
			if (!rules.get(k).isStartRule) continue;
			if (dp[0][input.length()][k] == null) continue;
			if (ret == null || dp[0][input.length()][k].score > ret.score) {
				ret = dp[0][input.length()][k];
			}
		}
		
		return ret;
	}
	
	private PotatoResult dpSegment(int left, int right, ArrayList<Unit> units) {
		PotatoResult[][] seg = new PotatoResult[right+1][units.size()+1];
		
		seg[left][0] = new PotatoResult();
		seg[left][0].score = 0;
		for (int i = left; i < right; i++) {
			for (int k = 0; k < units.size(); k++) { 
				if (seg[i][k] == null) continue;
				Unit unit = units.get(k);
				for (int j = i; j <= right; j++) {
					long oldScore = -PotatoResult.INF;
					if (seg[j][k+1] != null) oldScore = seg[j][k+1].score;
					
					if (unit.isTerminal()) {
						if (! (unit.name.equals(input.substring(i, j)) && seg[i][k].score > oldScore)) {
							continue;
						}
						seg[j][k+1] = seg[i][k].clone();
						if (unit.action != null) {
							seg[j][k+1].subSemantics.put(Integer.valueOf(unit.idInRule), unit.action); 
						}
					} else {
						PotatoResult pr;
						if (unit.isBuiltin()) {
							pr = this.extractorManager.getResult(i, j, unit);
						} else {
							dfs(i, j, ruleId.get(unit.name));
							pr = dp[i][j][ruleId.get(unit.name)];
						}
						if (! (pr != null && seg[i][k].score + pr.score > oldScore)) {
							continue;
						}
						seg[j][k+1] = seg[i][k].clone();
						seg[j][k+1].score += pr.score;
						seg[j][k+1].subParseResults.add(pr);
						seg[j][k+1].subSemantics.put(Integer.valueOf(unit.idInRule), pr.semantic);
					}
				}
			}
		}
		
		return seg[right][units.size()];
	}
	
	private void dfs(int i, int j, int k) {
		if (visited[i][j][k]) return;
		visited[i][j][k] = true;
		
		PotatoResult bestResult = null;
		Rule rule = rules.get(k);
		for (ArrayList<Unit> units : rule.expressions) {
			PotatoResult tmpResult = dpSegment(i, j, units);
			if (tmpResult == null) continue;
			for (Unit unit : units) tmpResult.triggeredSubRules.add(unit.name);
			if (bestResult == null || tmpResult.score > bestResult.score ) {
				bestResult = tmpResult;
			}
		}
		if (bestResult == null) {
			return;
		}
		bestResult.appliedRule = rule.name;
		bestResult.sentence = input.substring(i, j);
		// TODO(zlfengyi): Add code to support different system status which may affect score.
		if (rule.scores != null && rule.scores.containsKey("DEFAULT")) {
			bestResult.score += Integer.valueOf(rule.scores.get("DEFAULT").toString());
		}
	
		bestResult.semantic = applySemanticAction(bestResult, rule.action);
		dp[i][j][k] = bestResult;	
	}

	private void dpMain() {
		dp = new PotatoResult[input.length()+1][input.length()+1][rules.size()];
		visited = new boolean[input.length()+1][input.length()+1][rules.size()];
		
		// 记忆化搜索DP
		for (int i = input.length(); i >= 0; i--) {
			for (int j = i+1; j <= input.length(); j++) {
				// Use [i,j) to represent a segment.
				for (int k = 0; k < rules.size(); k++) {
					dfs(i, j, k);
				}
			}
		}
		
	}
	
	private Object applySemanticAction(PotatoResult pr, final JSONObject action) {
		// For semantic content after "!out" like: <size>::= 大!out({"size":大} | 小!out({"size":小});
		if (action == null || !action.containsKey("type")) {
			for (Object oj : pr.subSemantics.values()) {
				if (oj instanceof JSONObject) {
					if ("json_object".equals(((JSONObject)oj).get("type"))) {
						Object obj = ((JSONObject)oj).get("parameter");
						return obj;					
					}
					return oj;
				}
			}
			return null;
		}

		// For semantic content after "||".
		String type = action.get("type").toString(); 
		if ("variable".equals(type)) {
			int index = Integer.valueOf(action.get("parameter").toString());
			return pr.subSemantics.get(index);
		}
		// TODO(Yi, Yue): we should also support json_array?
		if ("json_object".equals(type)) {
			JSONObject ret = (JSONObject)JSONValue.parse(action.get("parameter").toString());
			
			for (Object key : ret.keySet()) {
				if (ret.get(key) instanceof JSONObject) {
					JSONObject v = (JSONObject)ret.get(key);
					if (v.containsKey("type")) ret.put(key, applySemanticAction(pr, v));
				}
			}
			
			return ret;
		}
		if ("function".equals(type)) {
			if ("merge".equals(action.get("name"))) {
				JSONObject ret = null;
				// TODO(Yue): change the List<Object> type to JSONArray.
				List<Object> ar = (List<Object>)action.get("parameter");
				for (Object oj : ar) {
					if (ret == null) {
						ret = (JSONObject)applySemanticAction(pr, (JSONObject)oj);
					} else {
						JSONObject added = (JSONObject)applySemanticAction(pr, (JSONObject)oj);	
						ret = JSONHelper.merge(ret, added);
					}
				}
				return ret;
			}
			// TODO(zlfengyi): Right now we only support AND JSONObject.
			// Make a more powerful AND operator between Array and Object.
			if("and".equals(action.get("name"))){
				JSONArray ret = new JSONArray();
				List<Object> ar = (List<Object>)action.get("parameter");
				for(Object oj : ar){
					ret.add((JSONObject)applySemanticAction(pr, (JSONObject)oj));
				}
				return ret;
			}
			
			if ("raw_text".equals(action.get("name"))) {
				return pr.sentence;
				/*
				JSONObject ret = new JSONObject();
				ret.put("value", pr.sentence);
				return ret;
				*/
			}
		}

		return null;
	}	
}
