package Potato.protos;

import java.util.*;

public class Expression {
	private boolean is_rule;
	private Rule rule;
	public ArrayList<String> ruleNames;
	private String command;

	public Expression(Rule r){
		is_rule = true;
		rule = r;
		command = "";
	}

	public Expression(){
		is_rule = false;
		command = "";
	}
	
	public Expression(String command, String ruleName){
		is_rule = false;
		if("start".equals(command)){
			this.command = "start";
			ruleNames = new ArrayList<String> ();
			ruleNames.add(ruleName);
		}
	}

	public boolean isRule(){
		return is_rule;
	}

	public Rule getRule(){
		return this.rule;
	}
	
	public boolean isStartStmt(){
		return (!is_rule) && command.equals("start");
	}
}