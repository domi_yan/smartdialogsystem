package Potato.protos;

import java.util.*;
import org.json.simple.JSONObject;

public class helper {
    public static ArrayList<ArrayList<Unit>> combine_2d_array_2d_array(ArrayList<ArrayList<Unit>> aar1, ArrayList<ArrayList<Unit>> aar2){
         ArrayList<ArrayList<Unit>> ret = new ArrayList<ArrayList<Unit>>();
         for(ArrayList<Unit> l1 : aar1) {
            for(ArrayList<Unit> l2 : aar2){
                ArrayList<Unit> temp = new ArrayList<Unit>();
                for(Unit t1: l1){
               		temp.add(t1);
               	}
               	for(Unit t2: l2){
                 	temp.add(t2);
               	}
                ret.add(temp);
            }      
         }
         return ret;
  }

	public static ArrayList<ArrayList<Unit>> concat_2d_array(ArrayList<ArrayList<Unit>> aar1, ArrayList<ArrayList<Unit>> aar2){
        ArrayList<ArrayList<Unit>> ret = new ArrayList<ArrayList<Unit>>();
        for(ArrayList<Unit> l1 : aar1) {
            ret.add(l1);
        }      
        for(ArrayList<Unit> l2 : aar2) {
            ret.add(l2);
        } 
         
        return ret;
    }

	public static void print2DArray(ArrayList<ArrayList<String>> aar){
   		for(ArrayList<String> ar:aar){
      		System.out.println(String.join(",", ar));
   		}
	}
	
	// This function do 3 things:
	// 1. merge rules of the same name (TODO: YY)
	// 2. mark rules can be a start rule 
	// 3. add namespace name to rule
	public static void add_name_space_name(ArrayList<Expression> ar, String space_name){
		HashSet<String> startRules = new HashSet<String>();
		//add start rules to hashset
		for(Expression e: ar){
			if(e.isStartStmt()){
				for(String name:e.ruleNames){
					startRules.add(name);
				}
			}
		}
		
		//add space name
		for(Expression e: ar){
			if (e.isRule()){
				Rule r = e.getRule();
				//check if is a start rule
				if (startRules.contains(r.name)){
					r.isStartRule = true;
				}
				
				r.setNameSpace(space_name);

				JSONObject obj = r.getAction();
				if(obj != null && obj.containsKey("symbols")){
					String symbols = (String)obj.get("symbols");
					String[] parts = symbols.split(",");
          
					for(int i = 0; i < parts.length; i++){
						parts[i] = parts[i].replace("$", "");
						if (parts[i].indexOf(".") == -1){
						// a local variable
							parts[i] = space_name + "." + parts[i];
							//System.out.println(parts[i]);
						}  
					}
					String new_symbol = String.join(", ", parts);
					obj.put("symbols", new_symbol);
					r.setAction(obj);
				}

				String old_name = r.RuleName();
				String new_name = helper.change_str(old_name, space_name);
				r.setRuleName(new_name);

				ArrayList<ArrayList<Unit>> aar = r.getExpressions();
				for(ArrayList<Unit> a:aar){
					for(int i = 0; i < a.size(); i++){
						Unit temp = a.get(i);
						String new_item_name = helper.change_str(temp.getName(), space_name);
						temp.setName(new_item_name);
						a.set(i, temp);
					}
				}
				r.setExpressions(aar);
				r.mark_parameter_in_json();
				//System.out.println(r);
			}
		}
	}
  public static boolean isParameter(String s){
    if(s.length() > 0 && s.charAt(0) == '$')
      return true;
    return false;
  }

  public static boolean parameterEqual(String str1, String str2){
    str1 = str1.replace("<", "").replace(">", "").replace("$", "");
    str2 = str2.replace("<", "").replace(">", "").replace("$", "");

    if(str1.equals(str2))
      return true;
    if(str1.indexOf(".") != -1){
      str1 = str1.split("\\.")[1];
    }
    if(str2.indexOf(".") != -1){
      str2= (str2.split("\\."))[1];
    }
    if(str1.equals(str2))
      return true;
    if(str1.indexOf("_") != -1){
      String [] parts = str1.split("_");
      str1 = parts[0];
      for(int i = 1; i < parts.length - 1; i++){
        str1 += "_" + parts[i];
      }
      if(!parts[parts.length-1].matches("[-+]?\\d*\\.?\\d+")){
        str1 += "_" + parts[parts.length-1];
      }

    }
    if(str2.indexOf("_") != -1){
      String [] parts = str2.split("_");
      str2 = parts[0];
      for(int i = 1; i < parts.length - 1; i++){
        str2+= "_" + parts[i];
      }
      if(!parts[parts.length-1].matches("[-+]?\\d*\\.?\\d+")){
        str2 += "_" + parts[parts.length-1];
      }
    }

    return str1.equals(str2);
  }

  public static String getUnitOrder(String str){
    if(str.indexOf("_") != -1){
      String [] parts = str.split("_");
      if(parts[parts.length - 1].matches("[-+]?\\d*\\.?\\d+"))
        return parts[parts.length - 1];
    }
    return "0";
  }

	public static String change_str(String str, String space_name){
		if (str == ""){
			return "";
		}
		int len = str.length();
		if(str.charAt(0) == '<' && str.indexOf('.') == -1){
			return "<" + space_name + "." + str.substring(1, len - 1) + ">";
		}
		else{
			return str;
		}

	}
}