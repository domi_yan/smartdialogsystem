package Potato.protos;

import java.util.*;
import org.json.simple.JSONObject;

public class ParseRule {
	private String name;
	private ArrayList<Unit> expands;
	private JSONObject action;
	private JSONObject scores;
	public ParseRule(String start, ArrayList<Unit> exprs, JSONObject a, JSONObject s){
		this.name = start;
		this.expands = exprs;
		this.action = a;
		this.scores = s;
	}
	
	public String getName(){
		return this.name;
	}
	
	public ArrayList<Unit> getExpands(){
		return this.expands;
	}
	
	public JSONObject getAction(){
		return this.action;
	}
	
	public JSONObject getScores(){
		return this.scores;
	}
	
	public String toString(){
		String ret = "";
		ret += this.name + " := ";
		for(Unit u:this.expands){
			ret += u.getName() + " #" + u.getRuleId();
		}
		return ret;
	}
}
