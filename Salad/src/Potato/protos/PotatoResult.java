package Potato.protos;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

// Final result output of GoodPotato
public class PotatoResult implements Cloneable {
	static final public long INF = (long)1e18; 
	
	public long score;
	
	// Semantic Object can be JSONObject or JSONArray.
	public Object semantic;
	
	public String sentence;
	
	public String appliedRule;

	public ArrayList<String> triggeredSubRules;
	
	public ArrayList<PotatoResult> subParseResults;

	/**
	 * For a expand rule like: <r> = <a>#1 <c>#3 <e>#5
	 * we store the sub semantics of #1, #3 and #5 in a hashmap.
	 */
	public HashMap<Integer, Object> subSemantics;

	public PotatoResult() {
		score = 0;
		triggeredSubRules = new ArrayList<String>();
		subParseResults = new ArrayList<PotatoResult>();
		subSemantics = new HashMap<Integer, Object>();
	}
	
	@Override
	public PotatoResult clone() {
		PotatoResult ret = new PotatoResult();
		ret.score = this.score;
		for (PotatoResult pr : this.subParseResults) {
			ret.subParseResults.add(pr);
		}
		for (Integer key : this.subSemantics.keySet()) {
			ret.subSemantics.put(key, this.subSemantics.get(key));
		}
		
		return ret;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Sentence: " + this.sentence +"\n");
		sb.append("Score: " + String.valueOf(this.score) + "\n");
		sb.append("Semantic: ");
		if (this.semantic != null) {
			sb.append(this.semantic.toString() + "\n");
		} else {
			sb.append("null\n");
		}
		sb.append("Applied Rule: " + this.appliedRule + "\n");
		sb.append("Triggered sub rules: ");
		for (String subRule: this.triggeredSubRules) {
			sb.append(subRule + ", ");
		}
		sb.append("\n");
		
		return sb.toString();
	}
}
