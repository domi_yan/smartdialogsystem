package Potato.protos;

import java.util.*;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

public class JSONHelper {
	static public JSONArray merge2JSONArray(JSONArray a1, JSONArray a2){
		//special case: empty array means no output
		//System.out.println("merge: " + a1.toString() + " " + a2.toString());
		
		if(a1.size() == 0)
			return a2;
		if(a2.size() == 0)
			return a1;

		JSONArray ret = new JSONArray();
		for(Object o1:a1){
			for(Object o2:a2){
				JSONObject o = new JSONObject();
				JSONArray a = new JSONArray();

				
				for(Iterator iterator = ((JSONObject)o1).keySet().iterator(); iterator.hasNext();) {
				    String key = (String) iterator.next();
				    //o.put(key, ((JSONObject)o1).get(key));
				}
				
				for(Iterator iterator = ((JSONObject)o2).keySet().iterator(); iterator.hasNext();) {
				    String key = (String) iterator.next();
				    if(o.containsKey(key)){
				    	Object t1 = o.get(key);
				    	Object t2 = ((JSONObject)o2).get(key);
				    	//o.put(key, merge2Object(key, t1, t2));
				    }
				    else{
				    	//o.put(key, ((JSONObject)o2).get(key));
				    }
				}
				
				
				ret.add(o);
			}
		}
		return ret;
	}
	
	static public Object merge2Object(String key, Object o1, Object o2){
		//System.out.println("merge2O " + key + " " + o1.toString() + o2.toString());
		Object obj = null;
		if(key.equals("scores")){
			obj = new JSONObject();
			for(Iterator iterator = ((JSONObject)o1).keySet().iterator(); iterator.hasNext();) {
			    String key1 = (String) iterator.next();
			    System.out.println("11 " + key1);
			    ((JSONObject)obj).put(key1, ((JSONObject)o1).get(key1));
			}
			for(Iterator iterator = ((JSONObject)o2).keySet().iterator(); iterator.hasNext();) {
			    String key1 = (String) iterator.next();
			    System.out.println("22 " + key1);
			    ((JSONObject)obj).put(key1, ((JSONObject)o2).get(key1));
			}
			System.out.println(obj);
			return obj;
		}
		return obj;
	}
	
	static public JSONObject getSemantic(JSONObject obj){
		if(obj == null)
			return new JSONObject();
		JSONObject o = deepCopyJSONObject(obj);
		
		if(o.containsKey("type") && o.get("type").equals("string")){
			return (JSONObject)o.get("raw_string");
		}
		
		JSONObject ret = new JSONObject();
		ret.put("semantic", o);
		return ret;
	}
	
	static public JSONObject mergeScore(JSONObject obj, JSONObject scoreObj){
		JSONObject o = deepCopyJSONObject(obj);
		JSONObject score = deepCopyJSONObject(scoreObj);
		if(score == null){
			score = new JSONObject();
			score.put("DEFAULT", 0);
		}
		o.put("score", score);

		return o;
	}
	
	static public JSONObject addExpr(JSONObject obj, ParseRule pr){
		JSONObject o = deepCopyJSONObject(obj);
		o.put("apply_rule", pr.getName());
		return o;
	}
	
	static public JSONObject addSentence(JSONObject obj, String s){
		obj.put("sentence", s);
		return obj;
	}
	
	static public JSONObject add2Action(JSONObject o1, JSONObject o2){
		if (o1.size() == 0){
			return o2;
		}
		if(o2.size() == 0){
			return o1;
		}
		return o1;
		
	}

	static public String pprint(JSONObject obj, int spaceNum){
		String ret = "";
		String blank = "";
		for(int i = 0; i < spaceNum; i++){
			blank += " ";
		}
		String space = "";
		for(int i = 0; i < 4; i++){
			space += " ";
		}
		ret += blank + "{\n";
		for(Iterator iterator = obj.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();

            if(obj.get(key) instanceof JSONObject) {
                JSONObject o = (JSONObject)obj.get(key);
                
                ret += blank + space + "\"" + key + "\":" + pprint(o, spaceNum + 4) + "\n";
            }
            else if(obj.get(key) instanceof List){
                ArrayList a = (ArrayList)obj.get(key);
                ret += blank + space + "\"" + key + "\":"  + "[\n";
                for(int i = 0; i < a.size(); i++){
                    JSONObject o = (JSONObject)a.get(i);
                    ret += pprint(o, spaceNum + 4);
                }
                ret += blank + space +"]\n";
            }
            else{
            	ret += blank + space + "\"" + key + "\":" + obj.get(key).toString() + "\n";
            }
		}
		ret += blank + "}\n";
		return ret;
		
	}

	static public JSONObject deepCopyJSONObject(JSONObject o){
 		String jsonText = JSONValue.toJSONString(o);
 		Object obj=JSONValue.parse(jsonText);
 		
 		return (JSONObject)obj;
	}
	
	static public boolean jsonEquals(Object o1, Object o2) {
		if (o1 == null && o2 == null) return true;
		if (o1 == null || o2 == null) return false;
		return JSONValue.toJSONString(o1).equals(JSONValue.toJSONString(o2));
	}
	
	static public JSONObject merge(JSONObject o1, JSONObject o2) {
		if (o1 == null) return o2;
		if (o2 == null) return o1;
		
		JSONObject ret = deepCopyJSONObject(o1);
		for (Object oKey : o2.keySet()) {
			String key = (String)oKey;
			if (o1.containsKey(key)) {
				// If the values are both JSONObject, we can do deep merge
				if (o1.get(key) instanceof JSONObject && o2.get(key) instanceof JSONObject) {
					ret.put(key, merge((JSONObject)o1.get(key), (JSONObject)o2.get(key)));
				} else {
					RuntimeException e = new RuntimeException("Conflict when merging two JSON objects");
					throw e;
				}
			} else {
				ret.put(key, o2.get(key));
			}
		}
		return ret;
	}
}
