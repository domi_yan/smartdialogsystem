package Potato.protos;

import java.util.*;
import java.util.HashSet;
import java.util.HashMap;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import util.GeneralUtil;

public class Rule {
    public String name;
    public ArrayList<ArrayList<Unit>> expressions;
    public ArrayList<Unit> units;
    public JSONObject action;
    public JSONObject scores;
    
    //can it be a start rule
    public boolean isStartRule;
    
    public String namespace;
    public HashMap unitMap;
    
    JSONParser parser;
    
    public Rule(String name, GrammarPart exprs){
        this.name = name;
        this.expressions = exprs.getExprs();
        this.units = exprs.getUnits();
        this.namespace = "UNKNOWN";
        this.isStartRule = false;
        deleteEmpty();
        action = null;
        scores = null;
        parser = new JSONParser();
        this.unitMap = new HashMap();
        generate_unit_map();
        
    }

    public Rule(Rule r){
        this.name = r.name;
        this.expressions = r.expressions;
        this.action = r.action;
        this.scores = r.scores;
        this.unitMap = r.unitMap;
        this.units = r.units;
        this.isStartRule = r.isStartRule;
    }

    public Rule(GrammarPart exprs){
        this("NOT SPECIFED", exprs);
    }

    public String getAllUnitsStr(){
        String ret = "";
        for(Unit u: units){
            ret += u.getName() + " #" +u.getRuleId() + "\n";
        }
        return ret;
    }

    public void mark_parameter_in_json(){
        JSONObject obj = this.action;
        
        if (obj != null)
            // mark parameter for Rule action
            // <a> ::= <b> {!!here!!}
            dfs_iterate_json(obj);
        
        
        for(Unit u: units){
            // mark parameter for Unit action
            // <a> ::= 狗!out({!!here!!}) | 猫!out({!!here!!})
            if(u.getAction() != null){
                JSONObject o = u.getAction();
                dfs_iterate_json(o);
                u.setAction(o);
            }
        }

    }

    private void dfs_iterate_json(JSONObject obj){
        for(Iterator iterator = obj.keySet().iterator(); iterator.hasNext();) {
            String key = (String) iterator.next();

            if(obj.get(key) instanceof JSONObject) {
                JSONObject o = (JSONObject)obj.get(key);
                dfs_iterate_json(o);
            }
            else if(obj.get(key) instanceof List){
                ArrayList a = (ArrayList)obj.get(key);
                for(int i = 0; i < a.size(); i++){
                    JSONObject o = (JSONObject)a.get(i);
                    dfs_iterate_json(o);
                }
            }
            else if (key.equals("parameter")){
                String str = (String)obj.get(key).toString();
                //{"parameter":"$order"} (variable)
                if(helper.isParameter(str)){
                    int count = 0;
                    for(Unit u: this.units){
                        if(helper.parameterEqual(str, u.getName())){
                            count += 1;
                            String str_order = helper.getUnitOrder(str);
                            if(Integer.toString(count).equals(str_order) || str_order.equals("0"))
                                    obj.put(key, u.getRuleId());
                        }
                    }
                }
                else{
                    for(Unit u: this.units){
                        //{"parameter":"ce959495-f746-4c5d-aa0e-e45863719656"} (unit ID)
                        if(str.equals(u.getId())){
                            obj.put(key, u.getRuleId());
                        }
                    }
                }
            }
        }
    }

    private void generate_unit_map(){
        int count = 1;
        for(Unit u: this.units){
            if(u.getRuleId() == null){
                this.unitMap.put(Integer.toString(count), u);
                u.setRuleId(Integer.toString(count));
                count ++;
            }
        }
    }

    private void deleteEmpty(){
        for(ArrayList<Unit> a:expressions){
            
        }
    }

    public ArrayList<ArrayList<Unit>> getRules(){
        return expressions;
    }

    public int size(){
        return expressions.size();
    }

    public HashSet<String> getSymbols() {
        HashSet<String> symbolsInvolved = new HashSet<>();
        if(this.action == null)
            return symbolsInvolved;
        if(this.action.get("action") == "output"){
            String symbols = (String)this.action.get("symbols");
            String[] parts = symbols.split(",");
            for(int i = 0; i < parts.length; i++){
                parts[i] = parts[i].replace("$", "");
                if (parts[i].indexOf(".") == -1){
                    //a local variable
                    parts[i] = this.namespace + "." + parts[i];    
                }
                symbolsInvolved.add(parts[i]);
            }
        }
        return symbolsInvolved;
    }

    // What's this??
    public JSONObject getActionForExpr(ArrayList<String> expr_names){
        if(this.action == null)
            return null;
        HashSet<String> symbols = getSymbols();
        ArrayList<String> ar = new ArrayList<String>();
        int count = 0;
        for(String name:expr_names){ 
            String temp = name.replace("<", "").replace(">", "");
            if(symbols.contains(temp)){
                count ++;
                ar.add(temp);
            }
        }

        if(count == symbols.size())
            return this.action;

        return null;
    }

    public String toString() {
        String ret = "";
        ret += name + "\n";
        ret += this.getAllUnitsStr();
        String prefix = name + " = ";
        for (ArrayList<Unit> ar:expressions){
            ArrayList<String> as = new ArrayList<String>();
            ArrayList<String> aj = new ArrayList<String>();
            for(Unit u:ar){
                as.add(u.getName());
                as.add("#" + u.getRuleId());
                if(u.getAction() != null){
                    aj.add(u.getAction().toString());
                }
            }

            JSONObject action = getActionForExpr(as);

            String r = String.join(" ", as);
            String j = "";
            if(aj.size() > 0){
                j = String.join(" ", aj);
            }
            ret += prefix + r + " " + j;
            if(action != null)
                ret += action.toString();

            if(this.scores != null){
                ret += "SCORE: " + this.scores.toString();
            }
            
            ret += "\n";
        }
        
        return ret;
    }

    public JSONObject getAction(){
        return this.action;
    }

    public void setAction(JSONObject obj){
        String s = obj.toString();
        try{
            Object o = parser.parse(s);
        }catch(ParseException pe){
            System.out.println("exception");
        }
        this.action = (JSONObject)obj;
    }

    public void setRuleName(String s) {
        this.name = s;
    }

    public String RuleName(){
        return this.name;
    }

    public void setNameSpace(String n){
        this.namespace = n;
    }

    public String getNameSpace(){
        return this.namespace;
    }

    public ArrayList<ArrayList<Unit>> getExpressions(){
        return this.expressions;
    }

    public void setExpressions(ArrayList<ArrayList<Unit>> aar){
        this.expressions = aar;
    }

    public JSONObject getScores(){
        return this.scores;
    }

    public void setScores(JSONObject obj){
        this.scores = obj;
    }

    public void addScore(JSONObject obj){
        if(this.scores == null){ 
            this.scores = new JSONObject();
        }
        for (String key : ((HashMap<String, Object>)(obj)).keySet()) {
            Object value = obj.get(key);
            this.scores.put(key, value);
            
        }
    }
}