package Potato.protos;

import java.util.*;

public class GrammarPart {
	private ArrayList<ArrayList<Unit>> aar;
	private ArrayList<Unit> units;

	public GrammarPart(ArrayList<Unit> n, ArrayList<ArrayList<Unit>> a){
		this.aar = a;
		this.units = n;
	}

	public ArrayList<Unit> getUnits(){
		return this.units;
	}

	public ArrayList<ArrayList<Unit>> getExprs(){
		return this.aar;
	}

	public void addExpr(ArrayList<Unit> ar){
		aar.add(ar);
	}

}