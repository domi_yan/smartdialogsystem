package Potato.protos;

import java.util.*;
import java.util.UUID;
import org.json.simple.JSONObject;

import util.GeneralUtil;


public class Unit {
	public String name;
	public JSONObject action;
	public String id;
	public String idInRule;
	public String parameter;
	
	public Unit(String n, JSONObject a){
		name = n;
		action = a;
		id = UUID.randomUUID().toString();
		idInRule = null;
		parameter = "";
		if(name.startsWith("<system") && name.indexOf('(') > 0){
			int leftIdx = name.indexOf('(');
			int rightIdx = name.lastIndexOf(')');
			parameter = name.substring(leftIdx + 1, rightIdx);
			name = "<" + name.substring(1, leftIdx) + ">";
		}
	}
	
	public Unit(String n){
		this(n, null);
	}
	
	public boolean equals(Unit unit) {
		if (!GeneralUtil.objectEquals(this.name, unit.name)) return false;
		if (!GeneralUtil.objectEquals(this.idInRule, unit.idInRule)) return false;
		if (!JSONHelper.jsonEquals(this.action, unit.action)) return false;
		
		return true;
	}
	
	// TODO(Yue): Move the logic to determinate terminal somewhere else, 
	// make this as a field.
	public boolean isTerminal() {
		if (name.charAt(0) == '<') return false;
		return true;
	}
	
	public boolean isBuiltin() {
		return name.startsWith("<system.");
	}

	public String getName(){
		return name;
	}

	public void setName(String n){
		this.name = n;
	}	

	public JSONObject getAction(){
		return this.action;
	}

	public void setAction(JSONObject obj){
		this.action = obj;
	}
	public String getId(){
		return this.id;
	}

	public void setRuleId(String s){
		this.idInRule = s;
	}
	
	public String toString(){
		return this.name + " #" + this.idInRule;
	}

	public String getRuleId(){
		return this.idInRule;
	}


}