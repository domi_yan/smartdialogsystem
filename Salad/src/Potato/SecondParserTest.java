package Potato;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import org.junit.Test;
import org.json.simple.JSONValue;

import Potato.protos.Rule;
import Potato.protos.PotatoResult;

public class SecondParserTest {
	public ArrayList<Rule> getRules(String inputStr) {
		FirstParser firstParser = new FirstParser();		
		return firstParser.getRulesFromStr(inputStr);
	}
	
	private void assertStringEqual(String s1, String s2) {
		s1 = s1.replace(" ", "").replace("\n", "");
		s2 = s2.replace(" ", "").replace("\n", "");
		assertEquals(s1, s2);
	}
	
	
	public void verifyPotatoResult(PotatoResult pr, 
			String triggeredSubRulesStr,
			int score,
			String semanticStr) {
		assertStringEqual(triggeredSubRulesStr, pr.triggeredSubRules.toString());
		assertEquals(score, pr.score);
		if(semanticStr == null){
			assertNull(pr.semantic);
		}
		else{
			semanticStr = JSONValue.parse(semanticStr).toString();
			assertStringEqual(semanticStr, pr.semantic.toString());
		}
	}
	
	@Test
	public void BasicTest1() {
		String inputRuleStr = "namespace order{  " +
				"   !start<order_food>;" +
				"   <food_name>::= 香辣鸡腿堡!out({\"obj_name\": \"香辣鸡腿堡\"})| 鸡腿堡!out({\"obj_name\":\"鸡腿堡\"}); " + 
				"   <order>::= 要|给我来|我要|我想要|| {\"intent\" : \"order\"}; " + 
				"   <order_food>::= <order><food_name> " + 
				"		|| MERGE($order, $food_name)!score(DEFAULT, 100); " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		
		String s = "我想要香辣鸡腿堡";
		String semanticStr = "{\"obj_name\":\"香辣鸡腿堡\",\"intent\":\"order\"}";
		String triggeredSubRulesStr = "[<order.order>,<order.food_name>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 100, semanticStr);
		
	}
	
	@Test 
	public void SentenceParsingTest1(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<s>; " +
				"	<a> ::= 我[们]; " + 
				"	<b> ::= 这个; " + 
				"	<c> ::= 人; " + 
				"	<d> ::= <e><f>; " + 
				"	<e> ::= 爱;  " + 
				"	<f> ::= 猫; " + 
				"	<s> ::= <a><b><c><d>; " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "我们这个人爱猫";
		String semanticStr = null;
		String triggeredSubRulesStr = "[<s.a>,<s.b>,<s.c>,<s.d>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	public void SemanticMergeTest1(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<s>; " +
				"	<a> ::= 我[们] || {\"who\":\"我\"}; " + 
				"	<b> ::= 这个 ; " + 
				"	<c> ::= 人 || {\"what\":\"人类\"}; " + 
				"	<d> ::= <e><f> || MERGE($e, $f); " + 
				"	<e> ::= 爱!out({\"action\":\"爱\"});  " + 
				"	<f> ::= 猫!out({\"animal\":\"猫\"}); " + 
				"	<s> ::= <a><b><c><d> || MERGE($a, $c, $d); " + 
				"} " + 
				"";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "我们这个人爱猫";
		String semanticStr = "{\"what\":\"人类\",\"action\":\"爱\",\"animal\":\"猫\",\"who\":\"我\"}";
		String triggeredSubRulesStr = "[<s.a>,<s.b>,<s.c>,<s.d>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	//<time> ::= [<year><month>][<day><hour><min>] || MERGE($year, $month, $day, $hour, $min);
	//Test if parser can ignore parameter (i.e. <day>) when not it is not used for parsing
	public void SemanticMergeTest2(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<time>; " +
				"	<year> ::= 二零一六 || {\"year\":\"2016\"}; " + 
				"	<month> ::= 一月 || {\"month\":\"Jan\"}; " + 
				"	<day> ::= 五日 || {\"day\": \"5th\"}; " + 
				"	<hour> ::= 七点 !out({\"hour\":\"7\"});  " + 
				"	<min> ::= 八分 !out({\"min\":\"8\"}); " +  
				"	<time> ::= [<year><month>][<day><hour><min>] || MERGE($year, $month, $day, $hour, $min); " + 
				"	 " + 
				"}" + 
				"";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "二零一六一月";
		String semanticStr = "{\"year\":\"2016\", \"month\":\"Jan\"}";
		String triggeredSubRulesStr = "[<s.year>, <s.month>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	//Test if parser can select the highest score
	public void ScoreTest1(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<r>; " +
				"	<r> ::= <a>|<b>|<c>; " + 
				"	<a> ::= 人 !score(\"DEFAULT\", 1); " + 
				"	<b> ::= 人 !score(\"DEFAULT\", 3); " + 
				"	<c> ::= 人 !score(\"DEFAULT\", 2);	 " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "人";
		String semanticStr = null;
		String triggeredSubRulesStr = "[<s.b>]";
		
		// Trigger rule <s.r>.
		assertEquals("<s.r>", p.getPotatoResult(s).appliedRule);
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 3, semanticStr);
	}
	
	@Test
	//Test if parser can match same symbol in different position correctly
	public void SemanticMergeTest3(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<report>; " +
				"	<team> ::= 皇马!out({\"team\":\"皇马\"})|巴萨!out({\"team\":\"巴萨\"})|湖人!out({\"team\":\"湖人\"}); " + 
				"	<vs> ::= 对; " + 
				"	<win> ::= 赢了 || {\"action\":\"win\"}; " + 
				"	<report> ::= <team><vs><team><team><win> || MERGE($team_3, $win); " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "皇马对巴萨湖人赢了";
		String semanticStr = "{\"action\":\"win\",\"team\":\"湖人\"}";
		String triggeredSubRulesStr = "[<s.team>,<s.vs>,<s.team>,<s.team>,<s.win>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	//Test if parser can read hard-coded JSON
	public void SemanticMergeTest4() {
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<report>; " +
				"	<team> ::= 皇马!out({\"team\":\"皇马\"});" + 
				"   <like> ::= 爱;" +
				"	<report> ::= <like><team> || MERGE({\"atitude\":\"like\"}, $team, {\"who\":\"unknown\"}); " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "爱皇马";
		String semanticStr = "{\"atitude\":\"like\",\"team\":\"皇马\",\"who\":\"unknown\"}";
		String triggeredSubRulesStr = "[<s.like>,<s.team>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}	
	
	@Test
	//Test if parser can merge empty variable.
	public void SemanticMergeTest5() {
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<report>; " +
				"	<year> ::= 二零一六!out({\"year\":\"2016\"}); " + 
				"	<month> ::= 一月!out({\"month\":\"Jan\"}); " +
				"   <date> ::= 六日!out({\"date\":\"6\"});" + 
				"	<report> ::= <year><month><date> ||  MERGE($year, MERGE($month, $date)); " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "二零一六一月六日";
		String semanticStr = "{\"date\":\"6\",\"month\":\"Jan\",\"year\":\"2016\"}";
		String triggeredSubRulesStr = "[<s.year>,<s.month>,<s.date>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	//if AND returns a JSONArray
	public void SemanticAndTest1(){
		String inputRuleStr =
				"namespace s{ " + 
				"   !start<two_player>; " +
				"	<player> ::= 科比!out({\"player_name\":\"科比\"})|萨克雷!out({\"player_name\":\"萨克雷\"}); " + 
				"	<and> ::= 和; " + 
				"   <two_player> ::= <player><and><player> || AND($player_1, $player_2);" + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "科比和萨克雷";
		String semanticStr = "[{\"player_name\":\"科比\"},{\"player_name\":\"萨克雷\"}]";
		String triggeredSubRulesStr = "[<s.player>,<s.and>,<s.player>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test 
	// MERGE($team, AND($player_1, $player_2))
	public void SemanticAndTest2(){
		String inputRuleStr =
				"namespace s{ " +
				"   !start<report>; " +
				"	<team> ::= 湖人!out({\"team\":\"湖人\"}); " + 
				"	<player> ::= 科比!out({\"player_name\":\"科比\"})|萨克雷!out({\"player_name\":\"萨克雷\"}); " + 
				"	<has> ::= 有; " + 
				"	<and> ::= 和; " + 
				"	<report> ::= <team><has><player><and><player> || MERGE($team,{\"who\":AND($player_1, $player_2)}); " + 
				"}";
		ArrayList<Rule> rules = getRules(inputRuleStr);
		SecondParser p = new SecondParser(rules);
		
		assertEquals(true, p.isRulesValid());
		String s = "湖人有科比和萨克雷";
		String semanticStr = "{\"team\":\"湖人\",\"who\":[{\"player_name\":\"科比\"},{\"player_name\":\"萨克雷\"}]}";
		String triggeredSubRulesStr = "[<s.team>, <s.has>, <s.player>, <s.and>, <s.player>]";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test 
	// || {"intent": "input_address", "value": $address}
	public void SemanticJSONTest1() { 
		String inputRuleStr = 
				"namespace takeout { " + 
				"	!start <input_live>; " + 
				"	<input_live>::= [<me>][<live>][<is>]<address> " + 
				"				|| MERGE({\"intent\": \"input_address\"}, $address); " + 
				"	<me>::= 我|我们|我们家|我家; " + 
				"	<is>::= 是; " + 
				"	<live>::= [的](住|住在|地址); " + 
				"	<address>::= 北京大学!out({\"value\": \"北京大学\"}) | 清华大学 !out({\"value\": \"清华大学\"}); " + 
				"}";
		SecondParser p = new SecondParser(FirstParser.getRulesFromStr(inputRuleStr));
		
		assertTrue(p.isRulesValid());
		String s = "我住北京大学";
		
		System.out.println(p.getPotatoResult(s));
		
		String triggeredSubRulesStr = "[<takeout.me>,<takeout.live>,<takeout.address>]";
		String semanticStr = "{\"intent\":\"input_address\",\"value\":\"北京大学\"}";
		verifyPotatoResult(p.getPotatoResult(s), triggeredSubRulesStr, 0, semanticStr);
	}
	
	@Test
	public void SystemRuleTest1() {
		String inputRuleStr = 
				"namespace takeout { " + 
				"	!start <input_address>;" +
				"   <input_address>::= <system.text(3)><beijing>" +
				"             || {\"value\": RAW_TEXT()} ; " +
				"   <beijing>::= 北京|北平|首都; " +
				"}";
		SecondParser p = new SecondParser(FirstParser.getRulesFromStr(inputRuleStr));
		assertTrue(p.isRulesValid());
		
		PotatoResult pr = p.getPotatoResult("哈哈哈北京");
		assertNotNull(pr);
		String semanticStr = "{\"value\":\"哈哈哈北京\"}";
		String triggeredSubRulesStr = "[<system.text>,<takeout.beijing>]";
		verifyPotatoResult(pr, triggeredSubRulesStr, 0, semanticStr);
		
		assertNull(p.getPotatoResult("哈和哈哈北京"));
	}
	
	@Test
	public void SystemRuleTest2() {
		String inputRuleStr = 
				"namespace takeout { " + 
				"	!start <input_address>;" +
				"   <input_address>::= <system.number><beijing>" +
				"             || $system.number; " +
				"   <beijing>::= 北京|北平|首都; " +
				"}";
		SecondParser p = new SecondParser(FirstParser.getRulesFromStr(inputRuleStr));
		assertTrue(p.isRulesValid());
		
		PotatoResult pr = p.getPotatoResult("12345北京");
		assertNotNull(pr);
		String semanticStr = "{\"number\": 12345}";
		String triggeredSubRulesStr = "[<system.number>,<takeout.beijing>]";
		verifyPotatoResult(pr, triggeredSubRulesStr, 0, semanticStr);
		
		assertNull(p.getPotatoResult("哈和哈哈北京"));
	}
}
