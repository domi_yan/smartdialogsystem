package Potato;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import org.junit.Test;

import Potato.protos.PotatoResult;
import Potato.protos.Rule;
import util.GeneralUtil;

// Potato integration test.
public class PotatoTest {
	

	public static void main(String[] args) {
		System.out.println("yes");
		String ruleStr = GeneralUtil.readWholeFile("../src/grammar/takeout.rul");
		
		
		ArrayList<Rule> rules = FirstParser.getRulesFromStr(ruleStr);
		
		SecondParser secondParser = new SecondParser(rules);
		secondParser.isRulesValid();
		
		System.out.println(secondParser.getPotatoResult("地址是清华大学"));
		System.out.println(secondParser.getPotatoResult("墨西哥鸡肉卷"));
		System.out.println(secondParser.getPotatoResult("我想要一个薯条"));
		System.out.println(secondParser.getPotatoResult("我想要一个薯条和二杯可乐"));
		System.out.println(secondParser.getPotatoResult("好的"));
	}
	
	@Test
	public void takeoutTest1() {
		String ruleStr = GeneralUtil.readWholeFile("src/grammar/takeout.rul");
		assertNotNull(ruleStr);
		
		ArrayList<Rule> rules = FirstParser.getRulesFromStr(ruleStr);
		assertNotNull(rules);
		
		SecondParser secondParser = new SecondParser(rules);
		assertTrue(secondParser.isRulesValid());
		
		System.out.println(secondParser.getPotatoResult("地址是清华大学"));
		System.out.println(secondParser.getPotatoResult("墨西哥鸡肉卷"));
		System.out.println(secondParser.getPotatoResult("我想要一个薯条"));
		System.out.println(secondParser.getPotatoResult("我想要一个薯条和二杯可乐"));
		System.out.println(secondParser.getPotatoResult("好的"));
	}
}
