package Potato.extractor;

import Potato.protos.NumAndPos;

public class ExtractUtil {
	static private int getCharNumber(char ch) {
		int ret = -1;
		if (Character.isDigit(ch)) {
			return ch - '0';
		} else {
			if (ch == '一') ret = 1;
			if (ch == '二') ret = 2;
			if (ch == '两') ret = 2;
			if (ch == '三') ret = 3;
			if (ch == '四') ret = 4;
			if (ch == '五') ret = 5;
			if (ch == '六') ret = 6;
			if (ch == '七') ret = 7;
			if (ch == '八') ret = 8;
			if (ch == '九') ret = 9;
			if (ch == '十') ret = 10;
			if (ch == '百') ret = 100;
		}
		return ret;
	}
	
	/**
	 * Get number start from the given index.
	 * @param input
	 * @param index
	 * @return
	 */
	static public NumAndPos getNumber(String input, int index) {
		NumAndPos ret = null;
		for (int i = index; i < input.length(); i++) {
	
			int v = getCharNumber(input.charAt(i));
			if (v == -1) break;
			if (ret == null) ret = new NumAndPos();
			
			if (v == 10) {
				ret.num *= 10;
				if (ret.num == 0) ret.num = 10;
			} else {
				ret.num = ret.num * 10 + v;
			}
			ret.pos = i+1;
		}
		return ret;
	}
	
	
}
