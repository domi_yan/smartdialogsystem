package Potato.extractor;

import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public interface Extractor {
	
	public void init(String _input);
	
	public PotatoResult getResult(int left, int right, Unit unit);
}
