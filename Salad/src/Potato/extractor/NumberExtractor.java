package Potato.extractor;

import org.json.simple.JSONValue;

import Potato.protos.NumAndPos;
import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public class NumberExtractor implements Extractor{
	private static final String RULE_NAME = "<system.number>";
	
	private String input;
	private PotatoResult[][] ret;
	
	private void preProcess() {
		ret = new PotatoResult[input.length()][input.length()+1];
		
		int i = 0;
		while (i < input.length()) {
			NumAndPos np = ExtractUtil.getNumber(input, i);
			if (np == null) {
				i++;
				continue;
			}
			PotatoResult pr = new PotatoResult();
			pr.semantic = JSONValue.parse("{\"number\" : " + np.num + "}");
			pr.appliedRule = RULE_NAME;
			pr.sentence = input.substring(i, np.pos);
			ret[i][np.pos] = pr;
					
			i = np.pos;
		}
	}
	
	@Override
	public void init(String _input) {
		this.input = _input;
		preProcess();
		
/*
		ret = new PotatoResult[input.length()][input.length()+1];
		// 抽取阿拉伯数字串
		{
			int i = 0;
			while (i < input.length()) {
				if (Character.isDigit(input.charAt(i)) ) {
					for (int j = i+1; j <= input.length(); j++) {
						if (j == input.length() || !Character.isDigit(input.charAt(j)) ) {	
							ret[i][j] = new PotatoResult();
							ret[i][j].semantic = JSONValue.parse("{\"number\" : " + input.substring(i, j) + "}");
							ret[i][j].appliedRule = RULE_NAME;
							ret[i][j].sentence = input.substring(i,j);
							i = j;				
							break;
						}
					}
				}
				i++;
			}
		}
		
		// 抽取单个中文数字：一，二，三...十
		for (int i = 0; i < input.length(); i++) {
			char ch = input.charAt(i);
			int value = -1;
			
			if (ch == '一') value = 1;
			if (ch == '二') value = 2;
			if (ch == '两') value = 2;
			if (ch == '三') value = 3;
			if (ch == '四') value = 4;
			if (ch == '五') value = 5;
			if (ch == '六') value = 6;
			if (ch == '七') value = 7;
			if (ch == '八') value = 8;
			if (ch == '九') value = 9;
			if (ch == '十') value = 10;
			if (value != -1) {
				PotatoResult pr = new PotatoResult();
				pr.semantic = JSONValue.parse("{\"number\" : " + value + "}");
				pr.appliedRule = RULE_NAME;
				pr.sentence = input.substring(i, i+1);
				ret[i][i+1] = pr;
			}
		}
		*/
		
	}

	@Override
	public PotatoResult getResult(int left, int right, Unit unit) {
		if (!(left >= 0 && left < right && right <= input.length()) ) {
			return null;
		}

		return ret[left][right];
	}
	
}
