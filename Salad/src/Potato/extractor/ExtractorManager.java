package Potato.extractor;

import java.util.HashMap;

import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public class ExtractorManager {
	HashMap<String, Extractor> extractors;

	public ExtractorManager() {
		this.extractors = new HashMap<String, Extractor>();
		this.extractors.put("text", new TextExtractor());
		this.extractors.put("number", new NumberExtractor());
	}

	public void init(String _input) {
		for (Extractor ex : extractors.values()) {
			ex.init(_input);
		}
	}

	public PotatoResult getResult(int left, int right, Unit unit) {
		// The name key should be string after "system.".
		String nameKey = unit.name.substring("<system.".length(), unit.name.length()-1);
		return extractors.get(nameKey).getResult(left, right, unit);
	}
}
