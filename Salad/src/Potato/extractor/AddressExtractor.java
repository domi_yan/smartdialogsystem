package Potato.extractor;

import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public class AddressExtractor implements Extractor {
	private String input;
	private PotatoResult dp[][];
	
	
	private void preProcess() {
		String[] words = {"市", "区", "小区", "街道", "街", "园", "大学", "楼", "室"};
		
	}
	
	@Override
	public void init(String _input) {
		this.input = _input;
		dp = new PotatoResult[input.length()][input.length()+1];
		
	}

	@Override
	public PotatoResult getResult(int left, int right, Unit unit) {
		if (left < 0 || left >= dp.length) return null;
		if (right < 0 || right >= dp[0].length) return null;
		return dp[left][right];
	}
	
	
}
