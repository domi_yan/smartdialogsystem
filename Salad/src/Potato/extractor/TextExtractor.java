package Potato.extractor;

import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public class TextExtractor implements Extractor {
	String input;
	
	@Override
	public void init(String _input) {
		this.input = _input;
	}
	
	@Override
	public PotatoResult getResult(int left, int right, Unit unit) {
		if (!(left >= 0 && left <= right && right <= input.length()) ) {
			return null;
		}
		if ((right-left) <= Integer.valueOf(unit.parameter)) {
			PotatoResult ret = new PotatoResult();
			ret.appliedRule = unit.name;
			ret.sentence = input.substring(left, right);
			ret.score = 0;
			
			return ret;
		} else {
			return null;
		}
	}

}
