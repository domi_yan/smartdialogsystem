package Potato.extractor;

import Potato.protos.NumAndPos;
import Potato.protos.PotatoResult;
import Potato.protos.Unit;

public class TimeExtractor implements Extractor {
	private String input;
	private Time[][] ret;
	
	public class Time { 
		int hour;
		int minute;
	}
	
	private void preProcess() { 
		boolean visited[] = new boolean[input.length()+1];
		
		// support form [早上|中午|下午|晚上|早|晚] +  
		// 6点 | 6点30 | 6点半 | 六点 | 六点四十 | 六点半 
		for (int i = 0; i < input.length(); i++) {
			
			NumAndPos np;
			Time t = new Time();
			if ((np = ExtractUtil.getNumber(input, i)) != null) {
				int nightHour = 0;
				
				if (np.pos < input.length() && input.charAt(np.pos) == '点') {
					t.hour = np.num;
					if (!(np.pos+1 < input.length())) continue;
					char ch = input.charAt(np.pos+1);
					if (Character.isDigit(ch)) {
						np = ExtractUtil.getNumber(input, np.pos+1);
						t.minute = np.num;
						ret[i][np.pos] = t;
						for (int j = i; j < np.pos; j++) visited[j] = true;
					} else if (ch == '半') {
						t.minute = 30;
						ret[i][np.pos+1] = t;
						for (int j = i; j < np.pos+1; j++) visited[j] = true;
					}
				}
			}
		}
				
	}
	
	@Override
	public void init(String _input) {
		input = _input;
		ret = new Time[input.length()][input.length()+1];
	}

	
	@Override
	public PotatoResult getResult(int left, int right, Unit unit) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
