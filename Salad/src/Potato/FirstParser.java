package Potato;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import Potato.protos.Rule;
import util.GeneralUtil;
 
 public class FirstParser { 
	 
	 private static List<String> findChineseChars(String content) {
			Pattern pattern = Pattern.compile("[\\u4e00-\\u9fa5]+");
			Matcher matcher = pattern.matcher(content);
			List<String> allMatches = new ArrayList<String>();
			while (matcher.find()){
				allMatches.add(matcher.group());
			}
			return allMatches;
	 }
	 
	 private static String preProcess(String content) {
		 content = content.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)", "");

			content = content.replaceAll("[ \n\t]*\\)[ \n\t]*", ")");
			content = content.replaceAll("[ \n\t]*\\([ \n\t]*", "(");
			content = content.replaceAll("[ \n\t]*\\>[ \n\t]*", ">");
			content = content.replaceAll("[ \n\t]*\\<[ \n\t]*", "<");
			content = content.replaceAll("[ \n\t]*\\][ \n\t]*", "]");
			content = content.replaceAll("[ \n\t]*\\[[ \n\t]*", "[");

			//Deal with Chinese Chractor
			List<String> ch_words = findChineseChars(content);
			for(String c:ch_words){
				content = content.replaceAll("\\>" + c, ">+" + c);
				content = content.replaceAll("\\)" + c, ")+" + c);
				content = content.replaceAll("\\]" + c, "]+" + c);
				content = content.replaceAll(c + "\\<", c + "+<");
				content = content.replaceAll(c + "\\(", c + "+(");
				content = content.replaceAll(c + "\\[", c + "+[");
			}

			content = content.replaceAll("\\>\\<", ">+<");
			content = content.replaceAll("\\>\\(", ">+(");
			content = content.replaceAll("\\>\\[", ">+[");


			content = content.replaceAll("\\)\\<", ")+<");
			content = content.replaceAll("\\)\\(", ")+(");
			content = content.replaceAll("\\)\\[", ")+[");

			content = content.replaceAll("\\]\\<", "]+<");
			content = content.replaceAll("\\]\\(", "]+(");
			content = content.replaceAll("\\]\\[", "]+[");

			//Delete empty line
			content = content.replaceAll("(?m)^[ \t]*\r?\n", "");
			
			return content;
	 }
	 
	 
	 public static ArrayList<Rule> getRulesFromFile(String fileName) {
		 String content = GeneralUtil.readWholeFile(fileName);
	     return getRulesFromStr(content);
	 }
	 
	 public static ArrayList<Rule> getRulesFromStr(String input) {
		 input = preProcess(input);
		 StringReader reader = new StringReader(input);
		 RuleLexer lexer = new RuleLexer(reader);
		 Parser p = new Parser(lexer); 
		 try {
			 p.parse();
			 return p.allRules;
		 } catch (Exception e) {
			 e.printStackTrace();
			 return null;
		 } 
	 }
}