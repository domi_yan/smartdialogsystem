package Potato;

import java.io.*;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.junit.Test;

import Potato.protos.Rule;
import Potato.protos.Unit;
import java_cup.runtime.*;

public class FirstParseTest {
	public void verifyRule(Rule rule, 
			String expectedUnitsOutput,
			String expectedExpsOutput,
			String expectedActionOutput,
			String expectedScoreOutput,
			boolean isStartRule
			) {
		assertStringEqual(expectedUnitsOutput, unitsToString(rule));
		assertStringEqual(expectedExpsOutput, expsToString(rule));
		if(expectedActionOutput == null){
			assertNull(rule.action);
		}
		else{
			//make sure the keys in same order
			expectedActionOutput = JSONValue.parse(expectedActionOutput).toString();
			assertStringEqual(expectedActionOutput, rule.action.toJSONString());
		}
		
		if(expectedScoreOutput == null){
			assertNull(rule.scores);
		}
		else{
			//make sure the keys in same order
			expectedScoreOutput = JSONValue.parse(expectedScoreOutput).toString();	
			assertStringEqual(expectedScoreOutput, rule.scores.toJSONString());
		}
		assertEquals(isStartRule, rule.isStartRule);
	}
	
	private String unitsToString(Rule rule) { 
		StringBuilder sb = new StringBuilder();
		for (Unit unit : rule.units) {
			sb.append(unit.name + "#" + unit.idInRule);
			if (unit.isTerminal()) sb.append("TMN");
			if (unit.action != null) sb.append(unit.action.toJSONString());
			
			sb.append(" ");
		}
		return sb.toString();
	}
	
	private String expsToString(Rule rule) {
		StringBuilder sb = new StringBuilder();
		for (ArrayList<Unit> units : rule.expressions) {
			sb.append(rule.name + " = ");
			for (Unit unit : units) {
				sb.append(unit.name + "#" + unit.idInRule);
			}
			sb.append("\n");
		}
		return sb.toString();
	}
	
	private ArrayList<Rule> getRuleFromStr(String str) {
		FirstParser firstParser = new FirstParser();
		return firstParser.getRulesFromStr(str);
	}
	
	private void assertStringEqual(String s1, String s2) {
		s1 = s1.replace(" ", "").replace("\n", "");
		s2 = s2.replace(" ", "").replace("\n", "");
		assertEquals(s1, s2);
	}

	@Test
	//test if first parser can assign the start rule correctly
	public void startRuleTest1() {
		String ruleStr = 
				"namespace a{" + 
			    "   !start <b>;"  +
			    "   !start <e>;"  +
				"	<b> ::= <c><d>;" + 
				"	<c> ::= 我; " +
				"	<d> ::= 你; " +
				"	<e> ::= 好的; " +
				"}";
		Rule rule1 = getRuleFromStr(ruleStr).get(0);

		String expectedUnits1 = "<a.c>#1 <a.d>#2";
		String expectedExps1 =
				"<a.b> = <a.c>#1 <a.d>#2";
		verifyRule(rule1, expectedUnits1, expectedExps1, null, null, true);
		
		Rule rule2 = getRuleFromStr(ruleStr).get(1);

		String expectedUnits2 = "我#1TMN";
		String expectedExps2 =
				"<a.c> = 我#1";
		verifyRule(rule2, expectedUnits2, expectedExps2, null, null, false);
		
		Rule rule3 = getRuleFromStr(ruleStr).get(2);

		String expectedUnits3 = "你#1TMN";
		String expectedExps3 =
				"<a.d> = 你#1";
		verifyRule(rule3, expectedUnits3, expectedExps3, null, null, false);
		
		Rule rule4 = getRuleFromStr(ruleStr).get(3);

		String expectedUnits4 = "好的#1TMN";
		String expectedExps4 =
				"<a.e> = 好的#1";
		verifyRule(rule4, expectedUnits4, expectedExps4, null, null, true);
	}
	
	@Test
	//use symbol from another namespace
	public void namespaceTest1() {
		String ruleStr = 
				"namespace a{" + 
				"	<b> ::= <c><d>;" + 
				"}" + 
				"namespace s{" + 
				"	<b> ::= <a.b><e>;" + 
				"}";
		Rule rule1 = getRuleFromStr(ruleStr).get(0);

		String expectedUnits1 = "<a.c>#1 <a.d>#2";
		String expectedExps1 =
				"<a.b> = <a.c>#1 <a.d>#2";
		verifyRule(rule1, expectedUnits1, expectedExps1, null, null, false);
		
		Rule rule2 = getRuleFromStr(ruleStr).get(1);

		String expectedUnits2 = "<a.b>#1 <s.e>#2";
		String expectedExps2 =
				"<s.b> = <a.b>#1 <s.e>#2";
		verifyRule(rule2, expectedUnits2, expectedExps2, null, null, false);
	}
	
	@Test
	//!!!
	public void ruleExpandTest1() {
		String ruleStr = "namespace s {"
				+ "  !start <r1>;"
				+ "  !start <r2>;"
				+ "  <r1>::=<a><b>;"
				+ "  <r2>::=<b><c>;"
				+ "}";
		// Test rule <r1>
		Rule rule1 = getRuleFromStr(ruleStr).get(0);

		String expectedUnits1 = "<s.a>#1 <s.b>#2";
		String expectedExps1 =
				"<s.r1> = <s.a>#1 <s.b>#2";
		verifyRule(rule1, expectedUnits1, expectedExps1, null, null, true);
		
		Rule rule2 = getRuleFromStr(ruleStr).get(1);

		String expectedUnits2 = "<s.b>#1 <s.c>#2";
		String expectedExps2 =
				"<s.r2> = <s.b>#1 <s.c>#2";
		verifyRule(rule2, expectedUnits2, expectedExps2, null, null, true);
	}
	
	// Test about "[]"
	@Test
	public void ruleExpandTest2() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= [<a><b>]<c>;"
				+ " }";

		//testOneCase(input, output);
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps =
				"<s.r> = <s.a>#1 <s.b>#2 <s.c>#3 "
			  + "<s.r> = <s.c>#3 ";
		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
		
		
	}
	
	// Test about "|" 
	@Test 
	public void ruleExpandTest3() {
		// "<b>|(<c><d>)" should equal to "<b>|<c><d>"
		String ruleStr1 =
				"   namespace s {"
				+ "   <r>::= <a>|(<b><c>);"
				+ "}";
		String ruleStr2 =
				"   namespace s {"
				+ "   <r>::= <a>|<b><c>;"
				+ "}";
		
		Rule rule1 = getRuleFromStr(ruleStr1).get(0);
		Rule rule2 = getRuleFromStr(ruleStr1).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
				" <s.r> = <s.a>#1 "
						+ " <s.r> = <s.b>#2 <s.c>#3 ";
		verifyRule(rule1, expectedUnits, expectedExps, null, null, false);
		verifyRule(rule2, expectedUnits, expectedExps, null, null, false);
	}

	// Test about "()" and "|" 
	@Test
	public void ruleExpandTest4() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>(<b>|<c>);"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
						" <s.r> = <s.a>#1 <s.b>#2 "
						+ " <s.r> = <s.a>#1 <s.c>#3 ";
		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
	}
	
	@Test
	public void ruleExpandTest5() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= [(<a>|<b>)<c>]<b>;"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1<s.b>#2<s.c>#3<s.b>#4";
		String expectedExps = 
				" <s.r> = <s.a>#1 <s.c>#3 <s.b>#4 "
				+ " <s.r> = <s.b>#2 <s.c>#3 <s.b>#4 "
				+ " <s.r> = <s.b>#4 ";
		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
	}
	
	@Test
	public void ruleExpandTest6() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= [<a>(<a>|<b>[<c>])] (<d>|<e>);"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = " <s.a>#1 <s.a>#2 <s.b>#3 <s.c>#4 <s.d>#5 <s.e>#6";
		String expectedExps = " <s.r> = <s.a>#1 <s.a>#2 <s.d>#5 "
						+ " <s.r> = <s.a>#1 <s.a>#2 <s.e>#6 "
						+ " <s.r> = <s.a>#1 <s.b>#3 <s.c>#4 <s.d>#5 "
						+ " <s.r> = <s.a>#1 <s.b>#3 <s.c>#4 <s.e>#6 "
						+ " <s.r> = <s.a>#1 <s.b>#3 <s.d>#5 "
						+ " <s.r> = <s.a>#1 <s.b>#3 <s.e>#6 "
						+ " <s.r> = <s.d>#5 "
						+ " <s.r> = <s.e>#6 ";

		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
	}
	
	@Test
	public void sementicOutTest1() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>(<b>|<c>)"
				+ "	   		|| $a;"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2\n" + 
				"<s.r> = <s.a>#1<s.c>#3";
		String expectedAction = 
				"{\"parameter\":\"1\",\"type\":\"variable\"}";
	
		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	// Note: always use parameter, since for function, [] can still be regarded as one parameter.
	@Test 
	public void sementicOutTest2() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>(<b>|<c>)"
				+ "	   		|| MERGE($a, $b);"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2\n" + 
				"<s.r> = <s.a>#1<s.c>#3";
		String expectedAction = 
				"{\"parameter\":" + 
		"[{\"parameter\":\"1\",\"type\":\"variable\"},{\"parameter\":\"2\",\"type\":\"variable\"}]," + 
		"\"name\":\"merge\",\"type\":\"function\"}";

		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	
	// Note: can't be parsed
	// Should throw an exception
	@Test(expected=Exception.class)
	public void sementicOutTest3() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>(<b>|<c>)"
				+ "	   		|| MERGE($a, AND({\"obj\": \"f1\"}, $b));"
				+ "}";
		
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2\n" + 
				"<s.r> = <s.a>#1<s.c>#3";
		String expectedAction = 
				"{\"parameter\":" + 
		"[{\"parameter\":\"1\",\"type\":\"variable\"}," + 
		"{\"parameter\":[{\"parameter\":{\"obj\":\"f1\"},\"type\":\"json_object\"}," + 
		"{\"parameter\":\"2\",\"type\":\"variable\"}]," + 
		"\"name\":\"and\",\"type\":\"function\"}],\"name\":\"merge\",\"type\":\"function\"}";

		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	@Test
	public void sementicOutTest4() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>(<b>|<c>)"
				+ "	   		|| MERGE($a, {\"key\" : AND({\"obj\": \"f1\"}, $b)});"
				+ "}";
		
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.c>#3";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2\n" + 
				"<s.r> = <s.a>#1<s.c>#3";
		String expectedAction = 
				"  {\"name\":\"merge\", \"type\":\"function\", \"parameter\": [ " 
				+ "   {\"parameter\":\"1\",\"type\":\"variable\"}, "
				+ "   {\"type\":\"json_object\", \"parameter\":{\"key\":{ "
				+ "       \"name\":\"and\", \"type\":\"function\", \"parameter\":[ " 
		        + "             {\"parameter\":{\"obj\":\"f1\"},\"type\":\"json_object\"},  "
		        + "             {\"parameter\":\"2\",\"type\":\"variable\"}],"
		        + "}}}]}";

		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	@Test
	public void sementicOutTest5() {
		String ruleStr = 
				"namespace takeout {\n" + 
				"	!start <input_live>;\n" + 
				"	<input_live>::= [<me>][<live>][<is>]<address>\n" + 
				"				|| {\"intent\": \"input_address\", \"value\": $address};\n" + 
				"}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<takeout.me>#1 <takeout.live>#2 <takeout.is>#3 <takeout.address>#4";
		String expectedExps = 
				"<takeout.input_live> = <takeout.me>#1 <takeout.live>#2 <takeout.is>#3<takeout.address>#4" + 
				"<takeout.input_live> = <takeout.me>#1 <takeout.live>#2 <takeout.address>#4 " + 
				"<takeout.input_live> = <takeout.me>#1 <takeout.is>#3 <takeout.address>#4 " +
				"<takeout.input_live> = <takeout.me>#1 <takeout.address>#4" +
				"<takeout.input_live> = <takeout.live>#2 <takeout.is>#3 <takeout.address>#4" +
				"<takeout.input_live> = <takeout.live>#2 <takeout.address>#4 " + 
				"<takeout.input_live> = <takeout.is>#3 <takeout.address>#4" + 
				"<takeout.input_live>=<takeout.address>#4";
		String expectedAction = 
				"{\"parameter\":{\"value\":{\"parameter\":\"4\",\"type\":\"variable\"}," +
			    "\"intent\":\"input_address\"},\"type\":\"json_object\"}";

		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, true);
	}
		
	@Test
	public void ruleNameMatchTest1() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a><b><a><a>"
				+ "			|| MERGE($a_1, $b, $a_3);"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.a>#3 <s.a>#4 ";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2<s.a>#3<s.a>#4\n";
		String expectedAction = 
				"{\"parameter\":"+
		"[{\"parameter\":\"1\",\"type\":\"variable\"}," + 
		"{\"parameter\":\"2\",\"type\":\"variable\"}," + 
		"{\"parameter\":\"4\",\"type\":\"variable\"}]," + 
		"\"name\":\"merge\",\"type\":\"function\"}";

		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	@Test
	public void ruleNameMatchTest2() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a><b><a>[<a>]"
				+ "			|| AND($a_1, $b, $a_3);"
				+ "}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		
		String expectedUnits = "<s.a>#1 <s.b>#2 <s.a>#3 <s.a>#4 ";
		String expectedExps = 
				"<s.r> = <s.a>#1<s.b>#2<s.a>#3<s.a>#4\n" + 
				"<s.r> = <s.a>#1<s.b>#2<s.a>#3";
		String expectedAction = 
				"{\"parameter\":"+
		"[{\"parameter\":\"1\",\"type\":\"variable\"}," + 
		"{\"parameter\":\"2\",\"type\":\"variable\"}," + 
		"{\"parameter\":\"4\",\"type\":\"variable\"}]," + 
		"\"name\":\"and\",\"type\":\"function\"}";
	
		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
		
	@Test
	public void terimnalRuleTest1() {
		String ruleStr = 
				"   namespace s {"
				+ "  <size>::= 大!out({\"size\": \"大\"})| 中!out({\"size\": \"中\"}) | 小!out({\"size\": \"小\"}); "
				+ " }";
		
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = 
				"大#1TMN{\"parameter\":{\"size\":\"大\"},\"type\":\"json_object\"}\n" + 
				"中#2TMN{\"parameter\":{\"size\":\"中\"},\"type\":\"json_object\"}\n" + 
				"小#3TMN{\"parameter\":{\"size\":\"小\"},\"type\":\"json_object\"}";
		String expectedExps = 
				"<s.size> = 大#1\n" + 
				"<s.size> = 中#2\n" + 
				"<s.size> = 小#3";
		
		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
	}
	
	@Test
	public void scoreTest1() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>|(<b><c>)"
				+ "		!score(DEFAULT, 10);"
				+ "}";
		
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a> #1 <s.b> #2 <s.c> #3";
		String expectedExps = "<s.r> = <s.a> #1" + "<s.r> = <s.b> #2 <s.c> #3";
		String expectedScore = "{\"DEFAULT\":10}";
		verifyRule(rule, expectedUnits, expectedExps, null, expectedScore, false);
		
	}
	
	@Test
	public void scoreTest2() {
		String ruleStr =
				"   namespace s {"
				+ "   <r>::= <a>|(<b><c>)"
				+ "		!score(DEFAULT, 10, YES_OR_NO, 100);"
				+ "}";
		
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<s.a> #1 <s.b> #2 <s.c> #3";
		String expectedExps = "<s.r> = <s.a> #1" + "<s.r> = <s.b> #2 <s.c> #3";
		String expectedScore = "{\"DEFAULT\":10, \"YES_OR_NO\":100}";
		verifyRule(rule, expectedUnits, expectedExps, null, expectedScore, false);
		
	}
	
	
	@Test
	public void simpleTakeout1() {
		String ruleStr = 
				"namespace takeout { \n" + 
				"   <order>::= 要|我想要|| {\"intent\" : \"order_food\"};\n" + 
				"}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "要#1 TMN 我想要#2 TMN";
		String expectedExps = 
				"   <takeout.order> = 要 #1"
				+ " <takeout.order> = 我想要 #2";
				
		String expectedAction = "{\"parameter\":{\"intent\":\"order_food\"},\"type\":\"json_object\"}";
		
		verifyRule(rule, expectedUnits, expectedExps,  expectedAction, null, false);
	}
	
	@Test
	public void simpleTakeout2() {
		String ruleStr = 
				"namespace takeout { \n" + 
				"   <food_name>::= 香辣鸡腿堡!out({\"obj_name\": \"香辣鸡腿堡\"})| 鸡腿堡!out({\"obj_name\":\"鸡腿堡\"});\n" + 
				"}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits 
		= "香辣鸡腿堡#1TMN{\"parameter\":{\"obj_name\":\"香辣鸡腿堡\"},\"type\":\"json_object\"}" +
		"鸡腿堡#2TMN{\"parameter\":{\"obj_name\":\"鸡腿堡\"},\"type\":\"json_object\"}";
		String expectedExps = "<takeout.food_name> = 香辣鸡腿堡#1\n" + 
				"<takeout.food_name> = 鸡腿堡#2";

		verifyRule(rule, expectedUnits, expectedExps, null, null, false);
		
	}
	
	@Test
	public void simpleTakeout3() {
		String ruleStr = 
				"namespace takeout { \n" + 
				"   <order_food>::= <order><food_name>\n" + 
				"			|| MERGE($order, $food_name)!score(DEFAULT, 100);\n" + 
				"}";
		Rule rule = getRuleFromStr(ruleStr).get(0);
		String expectedUnits = "<takeout.order>#1 <takeout.food_name>#2";
		String expectedExps = "<takeout.order_food> = <takeout.order>#1 <takeout.food_name>#2";
		String expectedAction = 
				"{\"type\" : \"function\", \"name\" : \"merge\", \"parameter\":\n" + 
				"[{\"parameter\":\"1\",\"type\":\"variable\"},{\"parameter\":\"2\",\"type\":\"variable\"}] }";

		String expectedScore = "{\"DEFAULT\" : 100 }";
		
		verifyRule(rule, expectedUnits, expectedExps, expectedAction, expectedScore, false);
	}
		
	@Test 
	public void systemRuleTest1() {
		String ruleStr = 
				"namespace ss { " +
				"   <tell_address>::= <live><system.address><system.text((abcd5))>; " +
				"}";

		Rule rule = getRuleFromStr(ruleStr).get(0);
		assertFalse(rule.units.get(0).isBuiltin());
		assertTrue(rule.units.get(1).isBuiltin());
		assertEquals("<system.address>", rule.units.get(1).name);
		assertTrue(rule.units.get(2).isBuiltin());
		assertEquals("<system.text>", rule.units.get(2).name);
		assertEquals("(abcd5)", rule.units.get(2).parameter);
	}
	
	@Test
	public void systemRuleTest2() {
		String ruleStr = 
				"namespace ss { " +
				"    <kw_place>::= 市|区|街道;" +
				"    <address>::= <system.text(3)>[<kw_place>]<system.text(3)> || RAW_TEXT(); " +
				"}";
		Rule rule = getRuleFromStr(ruleStr).get(1);
		String expectedUnits = "<system.text>#1 <ss.kw_place>#2 <system.text>#3";
		String expectedExps =
				"<ss.address> = <system.text>#1 <ss.kw_place>#2 <system.text>#3" +
				"<ss.address> = <system.text>#1 <system.text>#3";
		String expectedAction =
				"{\"type\": \"function\", \"name\":\"raw_text\"}";
		verifyRule(rule, expectedUnits, expectedExps, expectedAction, null, false);
	}
	
	@Test
	public void intergerTerminalTest1(){
		String ruleStr = 
				"namespace s { " +
				"   <a>::= \"个1人\"|\"1个人\"|\"我是一个人，我123爱祖国！。\"; " +
				"}";

		Rule rule = getRuleFromStr(ruleStr).get(0);

		String expectedUnits = "个1人#1TMN1个人#2TMN我是一个人，我123爱祖国！。#3TMN";
		String expectedExps =
				"<s.a>=个1人 #1 <s.a>=1个人 #2 <s.a>=我是一个人，我123爱祖国！。#3";

		verifyRule(rule, expectedUnits, expectedExps, null, null, false);	
	}
	
	public void outputOneCase(String input) {


	}
}