import java.util.ArrayList;
import java.util.Scanner;

import org.json.simple.JSONObject;

import Octopus.module.DialogModule;
import Octopus.module.SportModule;
import Octopus.module.TakeoutFsmModule;
import Octopus.visitor.OutputArg;
import Potato.FirstParser;
import Potato.SecondParser;
import Potato.protos.PotatoResult;
import Potato.protos.Rule;
import util.GeneralUtil;
import java.sql.*;

public class GoodSalad {
	private DialogModule module;
	private SecondParser parser;
	
	public GoodSalad(String rulName) {
		String ruleStr = GeneralUtil.readWholeFile(rulName);
		ArrayList<Rule> rules = FirstParser.getRulesFromStr(ruleStr);
		this.parser = new SecondParser(rules);
		this.parser.isRulesValid();	
		
		if (rulName.equals("takeout.rul")) {
			module = new TakeoutFsmModule();
		} else if (rulName.equals("sport.rul")) {
			module = new SportModule();
		} else {
			System.err.println(rulName + " is not supported.");
		}
	}
	
	public OutputArg getReply(String input) {
		input = input.replaceAll(" ", "");
		PotatoResult pr = parser.getPotatoResult(input);
		OutputArg ret = new OutputArg();
		if (pr != null && pr.semantic instanceof JSONObject) {
			System.out.println(pr.semantic);
			this.module.applyVisit((JSONObject)pr.semantic, ret);
		} else {
			this.module.applyVisit(null, ret);		
		}
		return ret;
	}
	
	public String getReplyForBesound(String inputStr) {
		BesoundUtil bu = new BesoundUtil();
		JSONObject obj = new JSONObject();
		obj.put("text", inputStr);
		JSONObject ret = bu.getReplyForBesound(obj);
		if(ret != null)
			return ret.toString();
		return "{}";
	}
	
	public static void main(String[] args) {
/*
		GoodSalad salad = new GoodSalad("takeout.rul");
		Scanner sc = new Scanner(System.in);
		while (1>0) { 
			OutputArg ret = salad.getReply(sc.nextLine());
			System.out.println(ret.responseString);
		}
		*/
		
		GoodSalad salad = new GoodSalad("sport.rul");
		JSONObject oj = new JSONObject();
		oj.put("text", "继续播放");
		System.out.println(salad.getReplyForBesound("继续播放").toString());
	}
}
