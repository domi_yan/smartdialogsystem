package Octopus.module;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

import Octopus.module.TakeoutFsmModule.TakeoutStatus;
import Octopus.visitor.OutputArg;
import Octopus.visitor.SportVisitor;
import Octopus.visitor.Visitor;

public class SportModule implements DialogModule {
	public String time;
	public int yardNumber;
	public String sport;
	public SportStatus status;
	//------------ comman variable for DialogModule --------------//
	public String failedResponse;
	public HashMap<SportStatus, ArrayList<String>> prompts;
	public int nPromptTimes;
	
	public SportModule() {
		yardNumber = 1;
		status = SportStatus.INIT;
		SportPromptsHelper.initPrompts(this);
	}
	
	public enum SportStatus {
		INIT,
		ASK_TIME,
		CONFIRM_WHOLE,
		ASK_CHANGE_SOMETHING,
		GOOD_END,
		CANCELED,
	}
	
	@Override
	public void applyVisit(JSONObject semantic, OutputArg outArg) {
		// TODO Auto-generated method stub
		Visitor v = new SportVisitor();
		v.visit(this, semantic, outArg);
	}
	
	
}
