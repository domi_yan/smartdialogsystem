package Octopus.module;

import java.util.ArrayList;
import java.util.HashMap;

import Octopus.module.TakeoutFsmModule.TakeoutStatus;

public class TakeoutPromptsHelper {
	
	private static ArrayList<String> putPrompt(TakeoutFsmModule m, TakeoutStatus status) {
		ArrayList<String> p = new ArrayList<String>();
		m.prompts.put(status, p);
		return p;
	}
	
	public static void initPrompts(TakeoutFsmModule m) {
		// failed response
		m.failedResponse = "不好意思，我还是没听明白你在说什么，我要请求开发者让我变的更聪明，在此期间您可以拨打人工客服123456?";
		
		m.prompts = new HashMap<TakeoutStatus, ArrayList<String>>();
		
		// for INIT
		ArrayList<String> p = putPrompt(m, TakeoutStatus.INIT);
		p.add("欢迎光临肯德基在线餐厅，我是点餐助手彬彬，请问您所在的城市是？");
		p.add("不好意思，这个城市不在我们的送餐范围内");
		
		// for ASK_ADDR
		p = putPrompt(m, TakeoutStatus.ASK_ADDR);
		p.add("好的，请问是%s具体什么地址？");
		p.add("请您输入更详细的送餐地址，比如黄海路123号");
		
		// for CONFIRM_ADDR
		p = putPrompt(m, TakeoutStatus.CONFIRM_ADDR);
		p.add("您的地址是：%s, 请问您确认么？");
		
		// for ASK_FOOD_WITH_ADDR
		p = putPrompt(m, TakeoutStatus.ASK_FOOD_WITH_ADDR);
		p.add("您输入的地址为：%s, 送达时间约需要25分钟，您要点什么呢?");
		p.add("请问您要点什么呢？例如您可以说 两份薯条， 一个香辣鸡腿堡");
		p.add("对不起我不太明白您点的菜品，我们的热门菜单有:\n 1.薯条 2.可乐 3.土豆泥 4.香辣鸡腿堡 5.老北京鸡肉卷 6.上校鸡块");
		
		// for CONFIRM_WHOLE
		p = putPrompt(m, TakeoutStatus.CONFIRM_WHOLE);
		p.add("您一共点了: %s 您的送餐地址是: %s, 确认下单吗?");
		p.add("不好意思，请问您是否确认下单呢？");
		
		// for GOOD_END
		p = putPrompt(m, TakeoutStatus.GOOD_END);
		p.add("恭喜您下单成功，彬彬祝您用餐愉快");
		
		// for CHANGE_SOMETHING
		p = putPrompt(m, TakeoutStatus.ASK_CHANGE_SOMETHING);
		p.add("请问您还有什么需要更改的吗？");
		p.add("请问您要改什么呢，比如您可以说： 更改地址或者是更改菜品");
		
		// for CHANGE_ADDRESS
		p = putPrompt(m, TakeoutStatus.ASK_CHANGE_ADDRESS);
		p.add("好的，请输入您要更换的地址");
		p.add("对不起，这好像不是一个地址，请重新输入您所在的地址");
		
		// for CHANGE_FOOD
		p = putPrompt(m, TakeoutStatus.ASK_CHANGE_FOOD);
		p.add("您需要更改什么菜品呢");
		p.add("我没有太明白您需要更改的菜品，您可以说 薯条改成大份的 或者 可乐改成两杯");
		
		// for ASK_FOOD_NUMBER
		p = putPrompt(m, TakeoutStatus.ASK_FOOD_NUMBER);
		p.add("请问您要几个%s呢");
		
		p.add("对不起，您还没有指定%s的数量，给您来一个好不好？");
		
	}
}
