package Octopus.module;

import org.json.simple.JSONObject;

import Octopus.visitor.OutputArg;

public interface DialogModule {
	public void applyVisit(JSONObject semantic, OutputArg outArg);
	
}
