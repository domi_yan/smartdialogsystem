package Octopus.module;

import java.util.ArrayList;
import java.util.HashMap;

import Octopus.module.SportModule.SportStatus;
import Octopus.module.TakeoutFsmModule.TakeoutStatus;

public class SportPromptsHelper {
	private static ArrayList<String> putPrompt(SportModule m, SportStatus status) {
		ArrayList<String> p = new ArrayList<String>();
		m.prompts.put(status, p);
		return p;
	}
	
	public static void initPrompts(SportModule m) {
		// failed response
		m.failedResponse = "不好意思，我还是没听明白你在说什么，我要请求开发者让我变的更聪明，在此期间您可以拨打人工客服123456?";
		
		m.prompts = new HashMap<SportStatus, ArrayList<String>>();
		ArrayList<String> p;
		
		// for INIT
		p = putPrompt(m, SportStatus.INIT);
		p.add("欢迎光临微风运动馆，我是预约助手彬彬，我们场馆提供网球、篮球、桌球和击剑四种运动场地，请问您要预约什么运动呢？");
		p.add("您要预约的运动场地我不是太懂，请问您想要什么运动呢？ 例如您可以回复 网球、篮球、桌球或者击剑");
		
		p = putPrompt(m, SportStatus.ASK_TIME);
		p.add("好的，您要预约的%s场地费用是每小时%d元，请问您要预约什么时间呢？");
		p.add("请输入您要预约的时间，比如您可以说 明天早上10点到11点。");
		
		p = putPrompt(m, SportStatus.CONFIRM_WHOLE);
		p.add("您预约了%s, %s的运动场地，要确认吗？");
	
		p = putPrompt(m, SportStatus.ASK_CHANGE_SOMETHING);
		p.add("请问您还有什么其他要求呢？您可以选择取消，或者更改预约的运动、时间、场地");
		
		p = putPrompt(m, SportStatus.GOOD_END);
		p.add("恭喜您预约成功，祝您运动愉快 身体健康！");
		
		p = putPrompt(m, SportStatus.CANCELED);
		p.add("已成功帮您取消预约， 欢迎您下次光临!");
	}
	
}
