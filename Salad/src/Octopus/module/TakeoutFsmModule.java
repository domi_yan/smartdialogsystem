package Octopus.module;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.simple.JSONObject;

import Octopus.visitor.OutputArg;
import Octopus.visitor.TakeoutFsmVisitor;

public class TakeoutFsmModule implements DialogModule {
	public String city;
	public String address;
	public HashMap<String, FoodItem> foods;
	public TakeoutStatus status;
	public HashMap<TakeoutStatus, ArrayList<String>> prompts;
	public int nPromptTimes;
	public String failedResponse;
	
	public TakeoutFsmModule() {
		foods = new HashMap<String, FoodItem>();
		status = TakeoutStatus.INIT;
		TakeoutPromptsHelper.initPrompts(this);
	}
	
	@Override
	public void applyVisit(JSONObject semantic, OutputArg outArg) {
		TakeoutFsmVisitor v = new TakeoutFsmVisitor();
		v.visit(this, semantic, outArg);
	}
	
	public enum TakeoutStatus {
		INIT,
		ASK_FOOD_WITH_ADDR,
		CONFIRM_WHOLE,
		GOOD_END,
		UNKNOW,
		ASK_CHANGE_SOMETHING,
		ASK_CHANGE_FOOD,
		ASK_CHANGE_ADDRESS,
		ASK_FOOD_NUMBER,
		CONFIRM_ADDR,
		ASK_ADDR,
		
	}
	
	public class FoodItem implements Serializable{
		public String name;
		public String size;
		public int amount;
		
		public FoodItem(String _name) {
			this.name = _name;
			this.amount = 0;
		}
		
		public String toString() {
			String ret = String.valueOf(amount);
			if (size != null) ret = ret + size;
			ret = ret + name;
			
			return ret;
		}
	}
	
	public FoodItem getFoodItem(String name) {
		if (foods.containsKey(name)) {
			return foods.get(name);
		} else {
			FoodItem food = new FoodItem(name);
			foods.put(name, food);
			return food;
		}
	}
	
	public String foodsToString() {
		StringBuilder ret = new StringBuilder();
		for (FoodItem food : foods.values()) {
			ret.append(food.toString() + ", ");
		}
		
		return ret.toString();
	}
}
