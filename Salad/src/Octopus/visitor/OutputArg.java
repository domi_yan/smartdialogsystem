package Octopus.visitor;

import java.util.HashSet;
import java.util.Set;


public class OutputArg {
	public String responseString;
	public boolean isResponsePreDefined;
	
	public String getResponseString() {
		return this.responseString;
	}

}
