package Octopus.visitor;

import java.util.ArrayList;

import org.json.simple.JSONObject;

import Octopus.SConsts;
import Octopus.module.DialogModule;
import Octopus.module.SportModule;
import Octopus.module.SportModule.SportStatus;
import Octopus.module.TakeoutFsmModule.TakeoutStatus;
import Octopus.module.TakeoutFsmModule;

public class SportVisitor implements Visitor {

	public void getDefaultOutput(SportModule m, OutputArg outArg) {
		ArrayList<String> p = m.prompts.get(m.status);
		if (p == null || m.nPromptTimes >= p.size()) {
			outArg.responseString = m.failedResponse;
			return;
		}
		outArg.responseString = p.get(m.nPromptTimes);
	}
	
	private void getOutput(SportModule m, OutputArg outArg) {
		if (outArg.isResponsePreDefined == false) {
			getDefaultOutput(m, outArg);
			
			if (m.status == SportStatus.ASK_TIME && m.nPromptTimes == 0) {
				int price = 60;
				if ("网球".equals(m.sport) || "击剑".equals(m.sport)) price = 80;
				outArg.responseString = String.format(outArg.responseString, m.sport, price);
			} else if (m.status == SportStatus.CONFIRM_WHOLE) {
				outArg.responseString = String.format(outArg.responseString, m.time, m.sport);
			}
			
		}
		m.nPromptTimes++;
	}

	public void jump(SportModule m, SportStatus status) { 
		m.status = status;
		m.nPromptTimes = 0;
	}
	private void transfer(SportModule m, JSONObject semantic, OutputArg outArg) {
		if (semantic == null) return;
		
		if (m.status == SportStatus.INIT) {
			if (semantic.containsKey(SConsts.SPORT)) {
				m.sport = (String)semantic.get(SConsts.SPORT);
				this.jump(m, SportStatus.ASK_TIME);
			}
		} else if (m.status == SportStatus.ASK_TIME) {
			if (semantic.containsKey(SConsts.TIME)) {
				m.time = (String)semantic.get(SConsts.TIME) + "，时间一小时";
				this.jump(m, SportStatus.CONFIRM_WHOLE);
			} else if (semantic.containsKey(SConsts.TIME_RANGE)) {
				m.time = (String)semantic.get(SConsts.TIME_RANGE);
				this.jump(m, SportStatus.CONFIRM_WHOLE);
			}
		} else if (m.status == SportStatus.CONFIRM_WHOLE) {
			if (SConsts.YES.equals(semantic.get(SConsts.YES_OR_NO))) {
				this.jump(m, SportStatus.GOOD_END);
			} 
			else if (SConsts.NO.equals(semantic.get(SConsts.YES_OR_NO))) {
				this.jump(m, SportStatus.ASK_CHANGE_SOMETHING);
			}
		} 
		if (SConsts.CANCEL.equals(semantic.get(SConsts.INTENT))) {
			this.jump(m, SportStatus.CANCELED);
		}
	}
	
	@Override
	public void visit(DialogModule m, JSONObject semantic, OutputArg outArg) {
		// TODO Auto-generated method stub
		this.transfer((SportModule)m, semantic, outArg);
		this.getOutput((SportModule)m, outArg);
	}

}
