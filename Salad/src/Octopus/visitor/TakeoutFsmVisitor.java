package Octopus.visitor;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import Octopus.SConsts;
import Octopus.module.DialogModule;
import Octopus.module.TakeoutFsmModule;
import Octopus.module.TakeoutFsmModule.FoodItem;
import Octopus.module.TakeoutFsmModule.TakeoutStatus;
import Octopus.AddressQueryHelper;
import Octopus.AddressQueryResult;


public class TakeoutFsmVisitor implements Visitor {
	public void getDefaultOutput(TakeoutFsmModule m, OutputArg outArg) {
		ArrayList<String> p = m.prompts.get(m.status);
		if (p == null || m.nPromptTimes >= p.size()) {
			outArg.responseString = m.failedResponse;
			return;
		}
		outArg.responseString = p.get(m.nPromptTimes);
	}
	
	public void getOutput(TakeoutFsmModule m, OutputArg outArg) {
		if (outArg.isResponsePreDefined == false) {
			getDefaultOutput(m, outArg);
			if (m.status == TakeoutStatus.ASK_ADDR){
				outArg.responseString = String.format(
						outArg.responseString, m.city);
			} else if(m.status == TakeoutStatus.CONFIRM_ADDR){
				outArg.responseString = String.format(
						outArg.responseString, m.address);
			} else if (m.status == TakeoutStatus.ASK_FOOD_WITH_ADDR && m.nPromptTimes == 0) {
				outArg.responseString = String.format(
						outArg.responseString, m.address);
			} else if (m.status == TakeoutStatus.CONFIRM_WHOLE && m.nPromptTimes == 0) {
				outArg.responseString = String.format(
						outArg.responseString, m.foodsToString(), m.address);
			} else if (m.status == TakeoutStatus.ASK_FOOD_NUMBER) {
				String emptyFoodStr = "";
				for (FoodItem f : m.foods.values()) if (f.amount == 0) {
					if (!emptyFoodStr.equals("")) emptyFoodStr += "和";
					emptyFoodStr += f.name;
				}
				outArg.responseString = String.format(outArg.responseString, emptyFoodStr);
			}
		}
		m.nPromptTimes++;
	}
	
	public void applyOrderFood(TakeoutFsmModule m, JSONObject semantic, JSONObject attr) {
		if (semantic.get(SConsts.NUMBER) != null) attr.put(SConsts.NUMBER, semantic.get(SConsts.NUMBER));
		if (semantic.get(SConsts.SIZE) != null) attr.put(SConsts.SIZE, semantic.get(SConsts.SIZE));
			
		// For form 1
		if (semantic.containsKey(SConsts.OBJ_NAME)) {
			// TODO(zlfengyi): What if obj.get("obj_name") returns null or invalid food name.
			FoodItem food = m.getFoodItem(semantic.get(SConsts.OBJ_NAME).toString());
			if (attr.get(SConsts.NUMBER) != null) {
				food.amount += Integer.valueOf(semantic.get(SConsts.NUMBER).toString());
			}
			if (attr.get(SConsts.SIZE) != null) {
				food.size = (String)semantic.get(SConsts.SIZE);
			}
		}
		// For form 2
		if (semantic.containsKey(SConsts.OBJ)) {
			for (Object oj : (JSONArray)semantic.get(SConsts.OBJ)) {
				applyOrderFood(m, (JSONObject)oj, attr);				
			}
		}
	}
	
	public void jump(TakeoutFsmModule m, TakeoutStatus status) { 
		m.status = status;
		m.nPromptTimes = 0;
	}
	
	public void transfer(TakeoutFsmModule m, JSONObject semantic, OutputArg outArg) {
		if (semantic == null) return;
		
		Object intent = semantic.get(SConsts.INTENT);
		Object yes_or_no = semantic.get(SConsts.YES_OR_NO);
		Object value = semantic.get(SConsts.VALUE);
		
		if (m.status == TakeoutStatus.INIT) {
			if (SConsts.INPUT_ADDRESS.equals(intent)) {
				String tempStr = (String)semantic.get("value");
				if(AddressQueryHelper.isCity(tempStr)){
					m.city = tempStr.replace("市", "");
					this.jump(m, TakeoutStatus.ASK_ADDR);
				}
			} 
		} else if (m.status == TakeoutStatus.ASK_ADDR){
			if (SConsts.INPUT_ADDRESS.equals(intent)) {
				m.address = AddressQueryHelper.combineAddr(m.address, (String)semantic.get("value"));
				System.out.println(m.address);
				try {
					System.out.println(m.city);
					AddressQueryResult r = AddressQueryHelper.getResult(m.city, m.address);
					System.out.println(r.level);
					System.out.println(r.formattedAddress);
					if(r.isPrecise){
						m.address = r.formattedAddress;
						this.jump(m, TakeoutStatus.CONFIRM_ADDR);
						outArg.isResponsePreDefined = true;
						outArg.responseString = String.format(
								"您的地址为%s。请问您确认么？", m.address);
					}
					else if(
							// find no place
							r.status == AddressQueryResult.Status.NO_ADDRESS ||
							// find place in other city
							r.formattedAddress.indexOf(m.city) == - 1){
						outArg.isResponsePreDefined = true;
						outArg.responseString = String.format(
								"对不起，我没有找到%s市的%s，请输入道路名称+门牌编号，例如：中关村北大街52号。", m.city, m.address);
					}
					else if("道路".equals(r.level) || "兴趣点".equals(r.level)){
						System.out.println("ahahhaha");
						outArg.isResponsePreDefined = true;
						outArg.responseString = String.format(
								"请问具体是%s多少号？", r.formattedAddress);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} 
		} else if (m.status == TakeoutStatus.CONFIRM_ADDR){
			Object oj = semantic.get(SConsts.YES_OR_NO);
			if (SConsts.YES.equals(oj)) {
				this.jump(m, TakeoutStatus.ASK_FOOD_WITH_ADDR);
			} else if(SConsts.NO.equals(oj)) {
				this.jump(m, TakeoutStatus.ASK_ADDR);
				m.address = "";
				outArg.isResponsePreDefined = true;
				outArg.responseString = String.format(
						"对不起，请您再次输入%s市的地址，要更加详细一些哦。", m.city);
				
				
			}
		} else if (m.status == TakeoutStatus.ASK_FOOD_WITH_ADDR) {
			if (semantic.containsKey(SConsts.OBJ) || semantic.containsKey(SConsts.OBJ_NAME)) {
				JSONObject attr = new JSONObject();
				applyOrderFood(m, semantic, attr);
				
				// check if there's food has no number;
				boolean noNumber = false;
				for (FoodItem f : m.foods.values()) {
					if (f.amount == 0) noNumber = true;
				}
				
				if (noNumber) {
					this.jump(m, TakeoutStatus.ASK_FOOD_NUMBER);
				} else if (m.foods.size() >= 1) {
					this.jump(m, TakeoutStatus.CONFIRM_WHOLE);
				} else {
					this.jump(m, TakeoutStatus.UNKNOW);
				}
			}
		} else if (m.status == TakeoutStatus.CONFIRM_WHOLE) {
			Object oj = semantic.get(SConsts.YES_OR_NO);
			if (SConsts.YES.equals(oj)) {
				this.jump(m, TakeoutStatus.GOOD_END);
			} else if(SConsts.NO.equals(oj)) {
				this.jump(m, TakeoutStatus.ASK_CHANGE_SOMETHING);
			}
		} else if (m.status == TakeoutStatus.ASK_CHANGE_SOMETHING) {
			if (SConsts.CHANGE_ADDRESS.equals(intent)) {
				this.jump(m, TakeoutStatus.ASK_CHANGE_ADDRESS);
			} else if (SConsts.CHANGE_FOOD.equals(intent)) {
				this.jump(m, TakeoutStatus.ASK_CHANGE_FOOD);
			} else if (SConsts.NO.equals(yes_or_no)) {
				this.jump(m, TakeoutStatus.GOOD_END);
			} else if (SConsts.ADD_FOOD.equals(intent)) {
				JSONObject attr = new JSONObject();
				applyOrderFood(m, semantic, attr);

				outArg.isResponsePreDefined = true;
				outArg.responseString = String.format(
						"好的，您更新的菜品为： %s 确认下单吗？", m.foodsToString());
				this.jump(m, TakeoutStatus.CONFIRM_WHOLE);
			}
		} else if (m.status == TakeoutStatus.ASK_CHANGE_ADDRESS) {
			if (semantic.containsKey(SConsts.VALUE)) {
				m.address = (String)semantic.get(SConsts.VALUE);
				this.jump(m, TakeoutStatus.CONFIRM_WHOLE);
				outArg.isResponsePreDefined = true;
				outArg.responseString = String.format(
						"好的，您更新的地址为: %s， 确认下单吗？", m.address);
			}
		} else if (m.status == TakeoutStatus.ASK_CHANGE_FOOD) {
			if (semantic.containsKey(SConsts.OBJ) || semantic.containsKey(SConsts.OBJ_NAME)) {
				JSONObject attr = new JSONObject();
				applyOrderFood(m, semantic, attr);
				
				this.jump(m, TakeoutStatus.CONFIRM_WHOLE);
				outArg.isResponsePreDefined = true;
				outArg.responseString = String.format(
						"好的，您更新的菜品为： %s 确认下单吗？", m.foodsToString());
			}
		} else if (m.status == TakeoutStatus.ASK_FOOD_NUMBER) {
			if (semantic.containsKey(SConsts.NUMBER)) {
				for (FoodItem f : m.foods.values()) {
					if (f.amount == 0) f.amount = Integer.valueOf(semantic.get(SConsts.NUMBER).toString());
				}
				this.jump(m, TakeoutStatus.CONFIRM_WHOLE);
			} else if (semantic.containsKey(SConsts.OBJ)) {
				JSONObject attr = new JSONObject();
				applyOrderFood(m, semantic, attr);
				this.jump(m, TakeoutStatus.CONFIRM_WHOLE);		
			}
		}
	}
	
	@Override
	public void visit(DialogModule m, JSONObject semantic, OutputArg outArg) {
		transfer((TakeoutFsmModule)m, semantic, outArg);
		getOutput((TakeoutFsmModule)m, outArg);
	}
}
