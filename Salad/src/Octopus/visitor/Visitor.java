package Octopus.visitor;

import org.json.simple.JSONObject;

import Octopus.module.DialogModule;
import Octopus.module.TakeoutFsmModule;

public interface Visitor {
	
	public void visit(DialogModule m, JSONObject semantic, OutputArg outArg);
	
}
