package Octopus;

// Constants for Octopus.
public class SConsts {
	
	public enum SystemStatus {
		DEFAULT,
		ASK_COMFIRM,
		ASK_ORDER_FOOD,
	}
	
	public enum ConfirmStatus {
		NOT_REQUIRED,
		REQUIRED,
		INPUT_YES,
		INPUT_NO,
		YES,
		NO,
		EMPTY
	}
	
	// String constants.
	public static String OBJ = "obj";
	
	public static String OBJ_NAME = "obj_name";
	
	public static String INTENT = "intent";
	
	public static String ORDER_FOOD = "order_food";
	
	public static String CHANGE_FOOD = "change_food";
	
	public static String INPUT_ADDRESS = "input_address";
	
	public static String INPUT_PHONE = "input_phone";
	
	public static String NUMBER = "number";
	
	public static String SIZE = "size";

	public static String YES_OR_NO = "yes_or_no";
	
	public static String VALUE = "value";
	
	public static String YES = "yes";
	
	public static String NO = "no";
	
	public static String SPORT = "sport";
	
	public static String TIME = "time";
	
	public static String TIME_RANGE = "time_range";
	
	public static String CANCEL = "cancel";
	
	public static String ADD_FOOD = "add_food";
	
	
	public static String CHANGE_ADDRESS = "change_address";
	
	public static String GENERAL_FOODS_MODULE = "GeneralFoodsModule";
	
	public static String GENERAL_CONFIRM_MODULE = "GeneralConfirmModule";
}
