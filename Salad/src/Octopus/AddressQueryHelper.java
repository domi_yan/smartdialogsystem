package Octopus;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

import Api.AmapAPI;
import Octopus.AddressQueryResult.Status;

public class AddressQueryHelper {
	static String[] citys = {"武汉", "北京", "上海", "杭州"};
	
	static public AddressQueryResult getResult(String cityStr, String s) throws Exception{
		String formattedAddress = "";
		String city = "";
		String district = "";
		String street = "";
		String number = "";
		String level = "";
		boolean isPrecise = false;
		AddressQueryResult.Status status = AddressQueryResult.Status.NULL;
		AddressQueryResult ret = new AddressQueryResult();
		ret.isPrecise = false;
		
		JSONObject obj = AmapAPI.getPosition(cityStr, s);
		if(AmapAPI.getNumLocation(obj) == 0){
			status = Status.NO_ADDRESS;
			ret.status = status;
			return ret;
		}
		else if(AmapAPI.getNumLocation(obj) > 1){
			status = Status.TOO_MANY_ADDRESSES;
			ret.status = status;
			return ret;
		}
		JSONObject location = (JSONObject)(((JSONArray) (obj.get("geocodes"))).get(0));
		if(location.get("formatted_address") instanceof String)
			formattedAddress = (String) location.get("formatted_address");
		Object o = location.get("city");
		if(o instanceof String){
			city = (String) o;
		}
		
		o = location.get("district");
		if(o instanceof String){
			district = (String) o;
		}
		
		o = location.get("street");
		if(o instanceof String){
			street = (String) o;
		}
		
		o = location.get("number");
		if(o instanceof String){
			number = (String) o;
		}
		
		o = location.get("level");
		if(o instanceof String){
			level = (String) o;
		}
		
		if(AmapAPI.isPreciseLocation(obj)){
			isPrecise = true;
		}

		if(status == AddressQueryResult.Status.NULL && number.equals("") ){
			status = Status.NO_NUMBER_INFO;
		}
		
		
		ret.formattedAddress = formattedAddress;
		ret.city = city;
		ret.district = district;
		ret.street = street;
		ret.number = number;
		ret.level = level;
		ret.status = status;
		ret.isPrecise = isPrecise;
		return ret;
	}
	
	static public boolean isCity(String s){
		s = s.replace("市", "");
		for(int i = 0; i < citys.length; i++){
			if(citys[i].equals(s))
				return true;
		}
		return false;
	}

	//TODO(YY):How to combine 2 address string
	static public String combineAddr(String s1, String s2){
		if(s1 == null)
			return s2;
		return s1 + s2;
	}
	
	
}
