package Octopus;

import Api.AmapAPI;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class AddressQueryResult {
	public enum Status{
		NO_ADDRESS,
		TOO_MANY_ADDRESSES,
		NO_CITY_INFO,
		NO_DISTRICT_INFO,
		NO_STREET_INFO,
		NO_NUMBER_INFO,
		COMPLETE,
		NULL,
	}
	
	public Status status;
	public String formattedAddress;
	public String city;
	public String district;
	public String street;
	public String number;
	public String level;
	public boolean isPrecise;
	
	AddressQueryResult() throws Exception{
	}
	
	public String toString(){
		String ret = "";
		ret += "formattedAddress: " + formattedAddress + "\n";
		ret += "city: " + city + "\n";
		ret += "districit: " + district + "\n";
		ret += "street: " + street + "\n";
		ret += "number: " + number + "\n";
		ret += "level: " + level + "\n";
		ret += "precise: " + isPrecise + "\n";
		
		return ret;
	}
}
