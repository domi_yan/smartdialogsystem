import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.JSONObject;

public class BesoundUtil {
	public JSONObject getReplyForBesound(JSONObject input) {
		if (!input.containsKey("text")) return null;
		String text = (String)input.get("text");
		
		JSONObject result = new JSONObject();
		Connection c = null;
		Statement stmt = null;
		try {
		      Class.forName("org.sqlite.JDBC");
		      c = DriverManager.getConnection("jdbc:sqlite:SongInfo.db");
		      stmt = c.createStatement();
		      ResultSet rs = stmt.executeQuery( "SELECT * FROM songs;" );
		      while (rs.next()) {
		    	  String name = rs.getString("name");
		    	  String singer = rs.getString("singer");
		    	  String album = rs.getString("album");
		    	 
		    	  if (text.indexOf(name) != -1 && name.length() > 1 && !name.equals("我想")) {
		    		  result.put("name", name);
		    	  }
		    	  if (text.indexOf(singer) != -1 && singer.length() > 1) {
		    		  result.put("singer", singer);
		    	  }
		    	  if (text.indexOf(album) != -1 && album.length() > 1) {
		    		  result.put("album", album);
		    	  }
		      }
		      if (result.values().size() > 0) {
		    	  result.put("action", "play");
		      }
		      
		      // for source..
		      Pattern p = Pattern.compile(
		    		  "(中国好声音|超级女声|快乐男声|我是歌手|中国好歌曲|中国之星|声动亚洲|歌声传奇|超级歌单)");
		      Matcher m = p.matcher(text);
		      if (m.find()) {
		    	  result.put("source", m.group());
		      }
		      
		      p = Pattern.compile(
		    		  "(台湾|小清新|轻音乐|劲爆|安静|激情|抒情|民歌|名族风|钢琴|复古|流行|经典|摇滚|乡村|古典)");
		      m = p.matcher(text);
		      if (m.find()) {
		    	  result.put("style", m.group());
		      }
		      
		} catch ( Exception e ) {
		      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		      System.exit(0);	      
	    }
		
		if (result.keySet().size() == 0) {
			if (Pattern.compile("(暂停|暂停播放|停止|停止播放)").matcher(text).find()) {
				result.put("action", "pause");
			}
			if (Pattern.compile("(继续|继续播放|接着)").matcher(text).find()) {
				result.put("action", "continue");
			}
			
			if (result.keySet().size() == 0) return null;
		}
		return result;
	}
}
